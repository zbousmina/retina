function n_pq = normalized_central_moment( p, q, A) 
%p = first order of moment 
%q = second order of moment 
%A = Modified vessel enhanced image (I_hu) 
%if there is any 0's in image replace, it with very small number %'eps'. 
A(A == 0) = eps; 
[m n] = size(A);
m00 = sum(sum(A)); 
%2-D Moment of order(p+q) 
m10 = 0; % m_pq where p=1 and q=0 
m01 = 0; % m_pq where p=0 and q=1 
for a = 1:m 
for b = 1:n 
m10 = m10 + (a) * A(a,b); 
m01 = m01 + (b) * A(a,b); 
end 
end 
% Central moment 
xx = m10/m00; 
yy = m01/m00; 
mu_00 = m00; 
mu_pq = 0; 
for ii = 1:m 
x = ii-xx; 
for jj = 1:n 
y = jj-yy; 
mu_pq = mu_pq + ((x)^p) * ((y)^q) * A(ii,jj); 
end 
end 
% Normalized cental moment of order(p+q) 
gamma = (0.5*(p+q))+1; 
temp = (mu_00^(gamma)); 
n_pq = mu_pq/temp; 
%if n_pq equals 0, assign it very less value 'eps' 
n_pq(n_pq == 0) = eps;

% Gray-Level-Based Features calculation 

function [f1, f2, f3, f4, f5] = gray_level_features(Ih) 

% fi => Features % f1(x,y) = Ih(x,y) - min{Ih(s,t)} 

fun1 = @(x) min(x(:)); 
Ih_min = nlfilter( Ih, [9 9], fun1); 
f1 = Ih - Ih_min; 
% f2(x,y) = max{Ih(s,t)} - Ih(x,y) 
fun2 = @(x) max(x(:)); 
Ih_max = nlfilter( Ih, [9 9], fun2); 
f2 = Ih_max - Ih; 
% f3(x,y) = Ih(x,y) - mean{Ih(s,t)} 
fun3 = @(x) mean(x(:)); 
Ih_mean = nlfilter( Ih, [9 9], fun3); 
f3 = Ih - Ih_mean; 
% f4(x,y) = Standard deviation{Ih(s,t)} 
fun4 = @(x) std(x(:)); 
Ih_std = nlfilter( Ih, [9 9], fun4); 
f4 = Ih_std; 
% f5(x,y) = Ih(x,y) 
f5 = Ih;

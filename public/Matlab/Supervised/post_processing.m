% Post Processing step 
function x = post_processing(I) 
%I = segmented image
fun =  @(iterative_fill) median(I(:));
fill = nlfilter(I, [3 3], fun );

final_vessel_segmented_image = bwareaopen(fill, 10); 
x = final_vessel_segmented_image;

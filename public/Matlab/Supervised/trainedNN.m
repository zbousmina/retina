% Training the NN 
% Loads train and target data 
load Drive_train_data_21.mat; 
load Drive_target_data_21.mat; 

%size(train_data)


%Training and target data are transposed to allow NN to take data as %a column vector 
train_data = train_data'; 
target_data = target_data'; 
netN = feedforwardnet(15); 

net = train(netN, train_data, target_data); 
mat_file = 'Drive_27_trainedNN.mat'; 
% Saving the trained NN into separate matfiles. 
save(mat_file, 'net', '-v6');

function f = moment_feature_vec_f6( I_ve) 
%I_ve is vessel enhanced image 
I_ve = double(I_ve); 
gauss_matrix = fspecial('gaussian', [17 17], 1.7); 
I_hu = I_ve .* gauss_matrix; 
f = moment_features(I_hu, 6);

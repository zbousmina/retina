%Perform simulation over the test data using trained NN 
%load test data and trained NN 
load Drive_test_data_40.mat;
load Drive_21_trainedNN.mat;
%read ground truth data 
org = imread('Images/DRIVE/training/images/40_training.tif'); 
gnd = imread('Images/DRIVE/training/1st_manual/40_manual1.gif'); 
 

[row col] = size(gnd); 
test_data = test_data';

% Simulation 
Y = net(test_data);

% For DRIVE 
threshold = (Y > 0.53); 
% For STARE 
%threshold = (Y > 0.43); 
threshold = double(threshold);
reshape_threshold_img = reshape(threshold, row, col); 
%final_segmented_image = post_processing(reshape_threshold_img); 

final_segmented_image = double(reshape_threshold_img); 
df = uint8(final_segmented_image)-gnd;
error = mse(uint8(final_segmented_image),gnd)
b = bwboundaries(final_segmented_image);

figure(1)     
set(gca ,'color',[1 1 1])
set(gcf ,'color',[1 1 1])

subplot(231),imshow(org);title('Original'),
          
subplot(232),imshow(final_segmented_image),title('Vessel Extraction'),
subplot(233),imshow(gnd),title('gnd'),
subplot(235),imshow(df),title(error),
subplot(236),imshow(org),title('Approximate'),
hold on
           for k = 1:numel(b)
               plot(b{k}(:,2), b{k}(:,1), 'G', 'Linewidth', 1)
           end
[Se, Sp, ppv, Npv, Acc] = performance_measure(final_segmented_image, gnd);

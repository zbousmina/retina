function out = FonctionFeature_generation(imgtoread,imagename)

pathimages='..\..\images\';
pathimages=strcat(pathimages,imgtoread);
image_file = pathimages;

%Proposed
I = imread(pathimages);
gnd = imread('Images/DRIVE/test/mask/03_test_mask.gif');
R=(I(:,:,1));
img1=10*R;
st = regionprops(img1, 'BoundingBox', 'Area' );
for ii= 1 : length(st)
   Areai(ii)= st(ii).Area;
end
[row col] = size(gnd); 
largest_blob_id= find(Areai==max(Areai));
Box=[st(largest_blob_id).BoundingBox(1),st(largest_blob_id).BoundingBox(2),...
	st(largest_blob_id).BoundingBox(3),st(largest_blob_id).BoundingBox(4)];

Imgcrop = imcrop(I,Box);   
RR = (I(:,:,1));
GG = (I(:,:,2));
BB = (I(:,:,3));
            
K=mean(mean(GG))/255
ginv = imcomplement(GG);           
adahist = adapthisteq(ginv);                         

se = strel('square',round(100*K));
gopen = imopen(adahist,se);       
godisk = adahist - gopen;
medfilt = medfilt2(godisk);

background = imopen(medfilt,strel('disk',10));
I2 = medfilt-background;                   
I3 = imadjust(I2);                     
level = graythresh(I3);
% Threshold                     
bw1 = im2bw(I3,0.67-K);
pathimages='..\..\images\resultProposed';
pathimages=strcat(pathimages,imagename,'.png');
imwrite(bw1,pathimages);
%HogProposed

image_file = pathimages; 
image = imread(image_file); 
[features,visualization] = extractHOGFeatures(image,'CellSize',[128 128]);

pathfile='..\..\Features\HogProposed';
pathfile=strcat(pathfile,imagename,'.txt');

csvwrite(pathfile,features);

%GLSM Proposed

image_file = pathimages; 
image = imread(image_file); 
image = imcomplement(image);
%imshow(image);

% Gray Level Based Feature Extraction 
%------------------------------------ 
[f1, f2, f3, f4, f5] = gray_level_features(image); 

f1_mean = mean2(f1); 
f2_mean = mean2(f2); 
f3_mean = mean2(f3); 
f4_mean = mean2(f4); 
f5_mean = mean2(f5); 

vect=[f1_mean,f2_mean,f3_mean,f4_mean,f5_mean];

pathfile='..\..\Features\GLSMProposed';
pathfile=strcat(pathfile,imagename,'.txt');

csvwrite(pathfile,vect);


%Supervised
pathimages='..\..\images\';
pathimages=strcat(pathimages,imgtoread);
image_file = pathimages;

image = imread(image_file); 
image = double(image)/255; 
mask1a = image(:,:,1) > (50/255); 
mask1 = imerode(mask1a, strel('disk',3)); 

green_channel_img = image(:,:,2); 
struct_elem = strel('disk',3,8); 
green_channel_img_open = double(imopen(green_channel_img,struct_elem)); %Iy 


mean_filt = fspecial('average',[3 3]); 
mean_filtered_img = filter2(mean_filt, green_channel_img_open); 
gauss_filter = fspecial('gaussian', [9 9], 1.8); 
conv_img = imfilter(mean_filtered_img, gauss_filter,'same'); 

mean_filt2 = fspecial('average',[69 69]); 
mean_filtered_img2 = filter2(mean_filt2, conv_img); % Ib 
mean_filtered_img2 = mean_filtered_img2 .* mask1; 
Isc = green_channel_img_open - mean_filtered_img2; %Isc=Iy-Ib 
Isc = Isc - min(Isc(:)); 
Isc = Isc / max(Isc(:)); 
mask_Isc = Isc .* mask1;

grayLevels = linspace(0,1,256); 
max_Isc = grayLevels(hist(Isc(:)) == max(hist(Isc(:)))); 
Ih = Isc + .5 - max_Isc; 
Ih(Ih<0) = 0; 
Ih(Ih>1) = 1; 

comp_Ih = imcomplement((Ih)); 
se = strel('disk', 8); 
top_hat_trans_comp_Ih = imtophat(comp_Ih, se); 

Ive = top_hat_trans_comp_Ih; 

pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'mask.jpg');
imwrite(mask1, pathimages);

pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'Geen.jpg');
imwrite(green_channel_img_open, pathimages);

pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'Mean.jpg');
imwrite(mean_filtered_img2, pathimages);

pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'Ih.jpg');
imwrite(Ih, pathimages);

pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'Ive.jpg');
imwrite(Ive, pathimages);


[f1, f2, f3, f4, f5] = gray_level_features(Ih); 

f6 = nlfilter(Ive, [17 17], @moment_feature_vec_f6); 
f7 = nlfilter(Ive, [17 17], @moment_feature_vec_f7); 
% Name matfiles as 'Drive_image_27_features.mat' 

mat_file = 'Drive_image_40_features.mat'; 
save(mat_file,'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', '-v6');



pathfile='..\..\Features\';
mat_file = strcat(pathfile,imagename,'.mat'); 


%load Drive_image_40_features.mat; 
f1_mean = mean2(f1); 
f2_mean = mean2(f2); 
f3_mean = mean2(f3); 
f4_mean = mean2(f4); 
f5_mean = mean2(f5); 
f6_mean = mean2(f6); 
f7_mean = mean2(f7); 
f1_std = std2(f1); 
f2_std = std2(f2); 
f3_std = std2(f3); 
f4_std = std2(f4); 
f5_std = std2(f5); 
f6_std = std2(f6); 
f7_std = std2(f7);

%Features normalized to zero mean and unit variance
new_f1 = (f1 - f1_mean)/f1_std; 
new_f2 = (f2 - f2_mean)/f2_std; 
new_f3 = (f3 - f3_mean)/f3_std; 
new_f4 = (f4 - f4_mean)/f4_std; 
new_f5 = (f5 - f5_mean)/f5_std; 
new_f6 = (f6 - f6_mean)/f6_std; 
new_f7 = (f7 - f7_mean)/f7_std; 
test_data = [new_f1(:) new_f2(:) new_f3(:) new_f4(:) new_f5(:) new_f6(:) new_f7(:)]; 

save(mat_file, 'test_data', '-v6');

load Drive_21_trainedNN.mat;

org = imread(image_file);
gnd = imread('Images/im0001.ah.ppm'); 
 

[row col] = size(gnd);
test_data = test_data';

% Simulation 
Y = net(test_data);

% For DRIVE 
% threshold = (Y > 0.53); 
% For STARE 
threshold = (Y > 0.43); 
threshold = double(threshold);
reshape_threshold_img = reshape(threshold, row, col); 
%final_segmented_image = post_processing(reshape_threshold_img); 

final_segmented_image = double(reshape_threshold_img); 
%df = uint8(final_segmented_image)-gnd;
%error = mse(uint8(final_segmented_image),gnd)
b = bwboundaries(final_segmented_image);

figure(1)     
set(gca ,'color',[1 1 1])
set(gcf ,'color',[1 1 1])


subplot(231),imshow(org);title('Original'),
          
subplot(232),imshow(final_segmented_image),title('Vessel Extraction'),
%subplot(235),imshow(df),title(error),
subplot(236),imshow(org),title('Approximate'),
hold on
           for k = 1:numel(b)
               plot(b{k}(:,2), b{k}(:,1), 'G', 'Linewidth', 1)
           end
pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'.png');
imwrite(final_segmented_image, pathimages);

image_file = pathimages; 
image = imread(image_file); 
[features,visualization] = extractHOGFeatures(image,'CellSize',[128 128]);

pathfile='..\..\Features\HogSupervised';
pathfile=strcat(pathfile,imagename,'.txt');

csvwrite(pathfile,features);

%GLSM Proposed

image_file = pathimages; 
image = imread(image_file); 
image = imcomplement(image);
%imshow(image);

% Gray Level Based Feature Extraction 
%------------------------------------ 
[f1, f2, f3, f4, f5] = gray_level_featuresSupervised(image); 

f1_mean = mean2(f1); 
f2_mean = mean2(f2); 
f3_mean = mean2(f3); 
f4_mean = mean2(f4); 
f5_mean = mean2(f5); 

vect=[f1_mean,f2_mean,f3_mean,f4_mean,f5_mean];

pathfile='..\..\Features\GLSMSupervised';
pathfile=strcat(pathfile,imagename,'.txt');

csvwrite(pathfile,vect);


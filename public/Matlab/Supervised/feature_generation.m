% Read image and generate mask 
%------------------------------ 
image_file = 'Images/DRIVE/test/images/20_test.tif'; 
image = imread(image_file); 
image = double(image)/255; 
mask1a = image(:,:,1) > (50/255); 
mask1 = imerode(mask1a, strel('disk',3)); 

green_channel_img = image(:,:,2); 
struct_elem = strel('disk',3,8); 
green_channel_img_open = double(imopen(green_channel_img,struct_elem)); %Iy 


mean_filt = fspecial('average',[3 3]); 
mean_filtered_img = filter2(mean_filt, green_channel_img_open); 
gauss_filter = fspecial('gaussian', [9 9], 1.8); 
conv_img = imfilter(mean_filtered_img, gauss_filter,'same'); 



mean_filt2 = fspecial('average',[69 69]); 
mean_filtered_img2 = filter2(mean_filt2, conv_img); % Ib 
mean_filtered_img2 = mean_filtered_img2 .* mask1; 
Isc = green_channel_img_open - mean_filtered_img2; %Isc=Iy-Ib 
Isc = Isc - min(Isc(:)); 
Isc = Isc / max(Isc(:)); 
mask_Isc = Isc .* mask1;

grayLevels = linspace(0,1,256); 
max_Isc = grayLevels(hist(Isc(:)) == max(hist(Isc(:)))); 
Ih = Isc + .5 - max_Isc; 
Ih(Ih<0) = 0; 
Ih(Ih>1) = 1; 

comp_Ih = imcomplement((Ih)); 
se = strel('disk', 8); 
top_hat_trans_comp_Ih = imtophat(comp_Ih, se); 

Ive = top_hat_trans_comp_Ih; 

% Gray Level Based Feature Extraction 
%------------------------------------ 
[f1, f2, f3, f4, f5] = gray_level_features(Ih); 

f6 = nlfilter(Ive, [17 17], @moment_feature_vec_f6); 
f7 = nlfilter(Ive, [17 17], @moment_feature_vec_f7); 
% Name matfiles as 'Drive_image_27_features.mat' 

mat_file = 'Drive_image_40_features.mat'; 
save(mat_file,'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', '-v6');

f1_mean = mean2(f1); 
f2_mean = mean2(f2); 
f3_mean = mean2(f3); 
f4_mean = mean2(f4); 
f5_mean = mean2(f5); 
f6_mean = mean2(f6); 
f7_mean = mean2(f7);
vect=[f1_mean,f2_mean,f3_mean,f4_mean,f5_mean,f6_mean,f7_mean];

csvwrite('..\..\DataSet\Means\20t_mean.txt',vect);

% Display
%------------------------------------ 
%subplot(241),imshow(image); title('origImg');
%subplot(242),imshow(image(:,:,1)); title('mask1');
%subplot(243),imshow(mask1); title('mask1');
%subplot(244),imshow(green_channel_img_open); title('mask1');
%subplot(245),imshow(mask_Isc); title('mask1');
%subplot(246),imshow(Ih); title('mask1');
%subplot(246),plot(vector); title('vector');

%return


% Train and test data 

load Drive_image_40_features.mat; 
f1_mean = mean2(f1); 
f2_mean = mean2(f2); 
f3_mean = mean2(f3); 
f4_mean = mean2(f4); 
f5_mean = mean2(f5); 
f6_mean = mean2(f6); 
f7_mean = mean2(f7); 
f1_std = std2(f1); 
f2_std = std2(f2); 
f3_std = std2(f3); 
f4_std = std2(f4); 
f5_std = std2(f5); 
f6_std = std2(f6); 
f7_std = std2(f7);

%Features normalized to zero mean and unit variance
new_f1 = (f1 - f1_mean)/f1_std; 
new_f2 = (f2 - f2_mean)/f2_std; 
new_f3 = (f3 - f3_mean)/f3_std; 
new_f4 = (f4 - f4_mean)/f4_std; 
new_f5 = (f5 - f5_mean)/f5_std; 
new_f6 = (f6 - f6_mean)/f6_std; 
new_f7 = (f7 - f7_mean)/f7_std; 
test_data = [new_f1(:) new_f2(:) new_f3(:) new_f4(:) new_f5(:) new_f6(:) new_f7(:)]; 

save('Drive_test_data_40.mat', 'test_data', '-v6');

%plot(new_f7)

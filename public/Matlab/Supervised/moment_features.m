%Moment features calculation 
function f = moment_features(I_hu, count) 
%I_hu is modified vessel enhanced image with a Gaussian filter 
%count == 6 for sixth feature and count == 7 for seventh feature 
if(count == 6) 
% First Moment 
n20 = normalized_central_moment(2, 0, I_hu); 
n02 = normalized_central_moment(0, 2, I_hu); 
phi_1 = n20 + n02; 
f = abs(log(phi_1)); 
elseif(count == 7) 
%Second Moment 
n20 = normalized_central_moment(2, 0, I_hu); 
n02 = normalized_central_moment(0, 2, I_hu); 
n11 = normalized_central_moment(1, 1, I_hu); 
phi_2 = (n20-n02)^2 + 4*(n11^2); 
f = abs(log(phi_2)); 

else 
error('Wrong input') 
end

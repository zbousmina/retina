% Main method 
function out = mainUnsupervisedMethod(imgtoread,imagename)
pathimages='..\..\images\';
pathimages=strcat(pathimages,imgtoread);
img = imread(pathimages);
img = img(:,:,2);
img = double(img)/255;
mask = img > (20/255);

% For Drive
mask = img > (35/255);

% For stare
%mask = double(imerode(mask, strel('disk',3)));

% Matched filtering
extractedVessel = matched_filtered(img, 1.2, 9, 12);

% Element-wise multiplication
newExtractedVessel = extractedVessel.*mask;

% Normalization
normVessel = newExtractedVessel -min(newExtractedVessel(:));
normVessel = normVessel / max(normVessel(:));normVessel = round(normVessel * 255);
grayValue = (unique(normVessel(:)))';grayValueCount = numel(grayValue);

c = grayValueCount;
t = graycomatrix(newExtractedVessel, 'GrayLimits',[min(newExtractedVessel(:)) max(newExtractedVessel(:))], 'NumLevels', c);
% Adding entire values of the co-occurence matrix
t_sum = sum(t(:));

% Normalizing the co-occurrence matrix
P = zeros(c,c);
for i = 1:c
    for j = 1:c
        P(i,j)= t(i,j)/t_sum;
    end
end

t_JRE = generate_Tjre(P, c);
% Thresholding

final_segmented_img = newExtractedVessel > t_JRE;

g_truth = imread('21_manual1.gif');

subplot(1,2,1), imshow(final_segmented_img),title('final_segmented_img');
subplot(1,2,2), imshow(g_truth),title('g_truth');
pathimages='..\..\images\result';
pathimages=strcat(pathimages,imagename,'.jpg');
imwrite(final_segmented_img, pathimages)

%performance_measurefunction is same as used in supervised method
[Se, Sp, Ppv, Npv, Acc] = performance_measure(final_segmented_img, g_truth);
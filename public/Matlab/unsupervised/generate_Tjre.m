function T = generate_Tjre( P, L)
%
% Probabilities associated with each quadrants
x = 1;
T_JRE = zeros(L);
for th = 1:L    
    PtB = sum(sum(P(1:th, th+1:L)));
    PtD = sum(sum(P(th+1:L, 1:th)));
    qtB = PtB / ((th+1)*(L-th-1));
    qtD = PtD / ((L-th-1)*(L+1));
    temp = -( PtB*log(qtB) + PtD*log(qtD))/10;
    if temp > 0
        T_JRE(x) = temp;
        x = x + 1;end
end
T = max(T_JRE(:));
end
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imagetraited extends Model
{
    protected $fillable = [
    	'ImageSource_id','image','Remarque','nbLigne','nbColonne','Features_oneFile','Features_TwoFile','Features_ThreeFile','Features_FourFile','Features_FiveFile','Features_SixFile','Features_SevenFile','FileMatPhath'
    ];
    public function imageSource()
    {
        return $this->belongsTo('App\ImageSource','ImageSource_id');
    }
    public function CbirResult()
    {
        return $this->hasMany('App\CbirResult');
    }
    public function Step()
    {
        return $this->hasMany('App\Step');
    }
    public function featureNewOne()
    {
        return $this->hasMany('App\FeatureNew');
        
    }
    public function featureNewTwo()
    {
        return $this->hasMany('App\FeatureNewTwo');
    }
    public function featureNewThree()
    {
        return $this->hasMany('App\FeatureNewThree');
    }
    public function featureNewFour()
    {
        return $this->hasMany('App\FeatureNewFour');
    }
    public function featureNewFive()
    {
        return $this->hasMany('App\FeatureFive');
    }
    public function featureNewSix()
    {
        return $this->hasMany('App\FeatureSix');
    }
    public function featureNewSeven()
    {
        return $this->hasMany('App\FeatureNewSeven');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CbirResult extends Model
{
    protected $fillable = [
    	'imageTreated_id','DataSet_id','ordre','distance','algo','color'
    ];
    public function imagetraited()
    {
        return $this->belongsTo('App\imagetraited','imageTreated_id');
    }
    public function Dateset()
    {
        return $this->belongsTo('App\DataSetImage','DataSet_id');
    }
}

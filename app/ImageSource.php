<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageSource extends Model
{
    protected $fillable = [
    	'Client_id','image','Remarque','finaleSegementeProposed','finaleSegementeSupervised','glsmSupervised','HogSupervised','glsmProposed','HogProposed'
    ];
    public function client()
    {
        return $this->belongsTo('App\Client','Client_id');
    }
    public function imageTraited()
    {
        return $this->hasMany('App\imagetraited');
    }
}

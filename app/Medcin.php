<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class Medcin extends User
{
    protected $fillable = [
    	'nom','prenom','email','telephone','mobile','fax','etat','password','titrePoste'
    ];
    public function patients()
    {
        return $this->hasMany('App\Client');
    }
}

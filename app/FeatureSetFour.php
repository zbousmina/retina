<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureSetFour extends Model
{
    protected $fillable = [
    	'DataSet_id','valeur','ligne','colonne'
    ];
    public function imagetraited()
    {
        return $this->belongsTo('App\DataSetImage','DataSet_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureNewThree extends Model
{
    protected $fillable = [
    	'imageTreated_id','valeur','ligne','colonne'
    ];
    public function imagetraited()
    {
        return $this->belongsTo('App\imagetraited','imageTreated_id');
    }
}

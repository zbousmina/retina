<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSetImage extends Model
{
    protected $fillable = [
    	'titre','image','GroundTruth','drive_remarque','doctor_Remarque','nbLigne','nbColonne','Features_oneFile','Features_TwoFile','Features_ThreeFile','Features_FourFile','Features_FiveFile','Features_SixFile','Features_SevenFile','FileMatPhath','GroundTruthProposed','color'
    ];
    public function CbirResult()
    {
        return $this->hasMany('App\CbirResult');
    }
    public function featureSetOne()
    {
        return $this->hasMany('App\FeatureSetOne');
        
    }
    public function featureSetTwo()
    {
        return $this->hasMany('App\FeatureSetTwo');
        
    }
    public function featureSetThree()
    {
        return $this->hasMany('App\FeatureSetThree');
        
    }
    public function featureSetFour()
    {
        return $this->hasMany('App\FeatureSetFour');
        
    }
    public function featureSetFive()
    {
        return $this->hasMany('App\FeatureSetFive');
        
    }
    public function featureSetSix()
    {
        return $this->hasMany('App\FeatureSetSix');
        
    }
    public function featureSetSeven()
    {
        return $this->hasMany('App\FeatureSetSeven');
        
    }

}

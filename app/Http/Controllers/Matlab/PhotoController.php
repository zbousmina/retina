<?php

namespace App\Http\Controllers\Matlab;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Client;
use App\ImageSource;
use App\imagetraited;
use App\DataSetImage;
use App\CbirResult;
use App\Step;


class PhotoController extends Controller
{
    function formViewSupevised($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }
        $client=Client::where('id','=',$id)->first();
        if ($client == null){
         return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }

        return view('image.getImageSupervised',array('client'=>$client));
    }

    function formView($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }
        $client=Client::where('id','=',$id)->first();
        if ($client == null){
         return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }

    	return view('image.getImage',array('client'=>$client));
    }
    function Traiterimage(Request $request,$id){
    	$credentials= $request->only('image','Remarque');
    	$rules = [
    		'image'=>'required|mimes:jpeg,png,jpg,gif,tif|max:10048',
            'Remarque' => 'required|string|max:5000'
    	];
    	$validator = \Validator::make($credentials,$rules);
        $result=array();
        // validate
        /*if ($validator->fails()) {
            $result['error']=$validator->messages();
            return response()->json($result, 200);
        }*/
        $handlresult=$this->handleImage($request,'image');
        $imageToTreate=$handlresult['new_name'];
        $imagename=$handlresult['filename'];
        //Matlab 
        $pwd = getcwd();
        $pwd=$pwd.'\Matlab\unsupervised';
        $lien=asset('images/'.$imageToTreate);
        $cmd = 'matlab -sd "' . $pwd . '" -r "mainUnsupervisedMethod(\''.$imageToTreate.'\',\''.$imagename.'\');exit" -wait -logfile log.txt';
        //$cmd = 'matlab -sd "' . $pwd . '" -r "mainUnsupervisedMethod();exit" -wait -logfile log.txt';
        $last_line = exec($cmd, $output, $retval);

        //endMatlab
        
    	$result['result']='result'.$imagename.'.png';
        $result['resultProposed']='resultProposed'.$imagename.'.png';
    	$result['image']=$imageToTreate;
        $imsgsrc=[
            'Client_id'=>$id,
            'image'=>$imageToTreate,
            'Remarque'=>$credentials['Remarque'],
            'finaleSegementeProposed'=>$result['resultProposed'],
            'finaleSegementeSupervised'=>$result['result']
        ];
        $iagesour=new ImageSource($imsgsrc);
        $iagesour->save();
        $imagtred=[
            'ImageSource_id'=>$iagesour->id,
            'image'=>$result['result'],
            'Remarque'=>"Unsupervised Methode"
        ];
        $iageTreaut=new imagetraited($imagtred);
        $iageTreaut->save();
    	return $result;
    }


    function GetFeatureExtractionSupervised(Request $request,$id){
        $credentials= $request->only('image','Remarque');
        $rules = [
            'image'=>'required|mimes:jpeg,png,jpg,gif,tif|max:10048',
            'Remarque' => 'required|string|max:5000'
        ];
        $validator = \Validator::make($credentials,$rules);
        $result=array();
        // validate
        /*if ($validator->fails()) {
            $result['error']=$validator->messages();
            return response()->json($result, 200);
        }*/
        $handlresult=$this->handleImage($request,'image');
        $imageToTreate=$handlresult['new_name'];
        $imagename=$handlresult['filename'];
        //Matlab 
        $pwd = getcwd();
        $pwd=$pwd.'\Matlab\Supervised';
        $lien=asset('images/'.$imageToTreate);
        $cmd = 'matlab -sd "' . $pwd . '" -r "FonctionFeature_generation(\''.$imageToTreate.'\',\''.$imagename.'\');exit" -logfile log.txt';
        //$cmd = 'matlab -sd "' . $pwd . '" -r "mainUnsupervisedMethod();exit" -wait -logfile log.txt';
        $last_line = exec($cmd, $output, $retval);

        //endMatlab
        $result['HogProposed']='Features/HogProposed'.$imagename.'.txt';
        $result['GLSMProposed']='Features/GLSMProposed'.$imagename.'.txt';
        $result['HogSupervised']='Features/HogSupervised'.$imagename.'.txt';
        $result['GLSMSupervised']='Features/GLSMSupervised'.$imagename.'.txt';
        $result['resultProposed']='resultProposed'.$imagename.'.png';
        $result['image']=$imageToTreate;
        $result['treatedImage']='result'.$imagename.'.png';
        $imsgsrc=[
            'Client_id'=>$id,
            'image'=>$imageToTreate,
            'Remarque'=>$credentials['Remarque'],
            'finaleSegementeProposed'=>$result['resultProposed'],
            'finaleSegementeSupervised'=>$result['treatedImage'],
            'glsmSupervised'=>$result['GLSMSupervised'],
            'HogSupervised'=>$result['HogSupervised'],
            'glsmProposed'=>$result['GLSMProposed'],
            'HogProposed'=>$result['HogProposed']
        ];
        $iagesour=new ImageSource($imsgsrc);
        $iagesour->save();
        $imagtred=[
            'ImageSource_id'=>$iagesour->id,
            'FileMatPhath'=>'',
            'image'=>$result['resultProposed'],
            'Remarque'=>"Supervised methode"
            
        ];
        $iageTreaut=new imagetraited($imagtred);
        $iageTreaut->save();
        $step=[
            'titre'=>'mask1',
            'image'=>'result'.$imagename.'mask.jpg',
            'imageTreated_id'=>$iageTreaut->id
        ];
        $StepSave=new Step($step);
        $StepSave->save();
        $step=[
            'titre'=>'Green Channel Image',
            'image'=>'result'.$imagename.'Geen.jpg',
            'imageTreated_id'=>$iageTreaut->id
        ];
        $StepSave=new Step($step);
        $StepSave->save();
        $step=[
            'titre'=>'Mean Filtered',
            'image'=>'result'.$imagename.'Mean.jpg',
            'imageTreated_id'=>$iageTreaut->id
        ];
        $StepSave=new Step($step);
        $StepSave->save();
        $step=[
            'titre'=>'Ih',
            'image'=>'result'.$imagename.'Ih.jpg',
            'imageTreated_id'=>$iageTreaut->id
        ];
        $StepSave=new Step($step);
        $StepSave->save();
        $step=[
            'titre'=>'Ive',
            'image'=>'result'.$imagename.'Ive.jpg',
            'imageTreated_id'=>$iageTreaut->id
        ];
        $StepSave=new Step($step);
        $StepSave->save();
        return $result;
    }



    function handleImage($request,$img){
        $image = $request->file($img);
        $imagefile=$image->getClientOriginalName();
        $filename = pathinfo($imagefile, PATHINFO_FILENAME);
        $rand=rand();
        $new_name = $rand . '.' . $image->getClientOriginalName();
        $image->move(public_path('images'), $new_name);
        //save the image
        $result=array();
        $result['new_name']=$new_name;
        $result['filename']=$rand.'.'.$filename;
       	return $result;
    }

    function handleTxtFile($filepath,$featurenumbrer){
        $myfile = fopen("D:\MATLAB\RetinalDetectionPFE-master\myFile.txt", "r") or die("Unable to open file!");
        $filesize=filesize($myfile);
        dd($filesize);
        //echo fread($myfile,filesize("webdictionary.txt"));
        fclose($myfile);
    }
    function handleTxtFileTest(){
        $myfile = fopen("D:\MATLAB\RetinalDetectionPFE-master\myFile.txt", "r") or die("Unable to open file!");
        $filesize=filesize("D:\MATLAB\RetinalDetectionPFE-master\myFile.txt");
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $nbligne=sizeof($vector);
        $nbligne=$nbligne-1;
        $ligneone=explode(",", $vector[0]);
        $nbCologne=sizeof($ligneone);
        //floatval($var);
        //dd($ligneone[0]);
        for ($i=0; $i <$nbligne ; $i++) { 
            $ligneone=explode(",", $vector[$i]); 
            for ($j=0; $j <$nbligne ; $j++) { 
                  
              }  
        }
        //echo fread($myfile,filesize("webdictionary.txt"));
        fclose($myfile);
    }

    function TifTojpg(){
        $dataset=DataSetImage::get();
        $propos="DataSet/Features/ProposedFeatures/";
        $Supervi="DataSet/Features/SupervisedFeatures/";
        $GrounTruProposed="DataSet/Images/Proposed/";
        $GrounTru="DataSet/Images/Supervised/";
        $grtPro="";
        $grtSuper="";
        $propGLSM="";
        $propHOG="";
        $SuperviGLSM="";
        $SuperviHOG="";
        $j=1;
        for ($i=0; $i <sizeof($dataset) ; $i++) { 
            $j=$i+1;
            if ($j<10){
                $propGLSM=$propos.'img0'.$j.'_GLCM.txt';
                $propHOG=$propos.'img0'.$j.'_HOG.txt';
                $SuperviGLSM=$Supervi.'img0'.$j.'_GLCM.txt';
                $SuperviHOG=$Supervi.'img0'.$j.'_HOG.txt';
                $grtPro=$GrounTruProposed.'img0'.$j.'.png';
                $grtSuper=$GrounTru.'img0'.$j.'.png';
            }else{
                $propGLSM=$propos.'img'.$j.'_GLCM.txt';
                $propHOG=$propos.'img'.$j.'_HOG.txt';
                $SuperviGLSM=$Supervi.'img'.$j.'_GLCM.txt';
                $SuperviHOG=$Supervi.'img'.$j.'_HOG.txt';
                $grtPro=$GrounTruProposed.'img'.$j.'.png';
                $grtSuper=$GrounTru.'img'.$j.'.png';
            }
           /* $dataset[$i]->Features_oneFile=$propGLSM;
            $dataset[$i]->Features_TwoFile=$propHOG;
            $dataset[$i]->Features_ThreeFile=$SuperviGLSM;
            $dataset[$i]->Features_FourFile=$SuperviHOG;
            $dataset[$i]->GroundTruth=$grtSuper;
            $dataset[$i]->GroundTruthProposed=$grtPro;*/
            if ($dataset[$i]->drive_remarque=="not show any sign of diabetic retinopathy"){
                $dataset[$i]->color=0;
            }else{
                $dataset[$i]->color=1;
            }
            $dataset[$i]->save();
        }
        return $dataset;
    }

    function CalculeDistanceEuclidienne($fileOne,$FileTwo){
        $myfile = fopen($fileOne, "r") or die("Unable to open file!");
        $filesize=filesize($fileOne);
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $nbligne=sizeof($vector);
        $nbligne=$nbligne-1;
        $v1=array();
        for ($i=0; $i <$nbligne ; $i++) { 
            $ligneone=explode(",", $vector[$i]); 
            $ligneone=array_filter($ligneone);
            $v1[$i]=array_sum($ligneone)/sizeof($ligneone);
        }
        fclose($myfile);
        $myfile = fopen($FileTwo, "r") or die("Unable to open file!");
        $filesize=filesize($FileTwo);
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $nbligne=sizeof($vector);
        $nbligne=$nbligne-1;
        $v2=array();
        for ($i=0; $i <$nbligne ; $i++) { 
            $ligneone=explode(",", $vector[$i]); 
            $ligneone=array_filter($ligneone);
            $v2[$i]=array_sum($ligneone)/sizeof($ligneone);
        }
        fclose($myfile);
        //Calcule Distance
        $sum=0;
        for ($i=0; $i <$nbligne ; $i++) { 
            $sum=$sum+pow(($v1[$i]-$v2[$i]),2);
        }
        $sum=sqrt($sum);
        return $sum;  
    }
    function CalculeDistanceEuclidienneTest(){
        $myfile = fopen("D:\MATLAB\RetinalDetectionPFE-master\myFile.txt", "r") or die("Unable to open file!");
        $filesize=filesize("D:\MATLAB\RetinalDetectionPFE-master\myFile.txt");
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $nbligne=sizeof($vector);
        $nbligne=$nbligne-1;

        $v1=array();
        for ($i=0; $i <$nbligne ; $i++) { 
            $ligneone=explode(",", $vector[$i]); 
            $ligneone=array_filter($ligneone);
            $v1[$i]=array_sum($ligneone)/sizeof($ligneone);
        }
        fclose($myfile);
        $myfile = fopen('D:\MATLAB\RetinalDetectionPFE-master\21_f1_mat.txt', "r") or die("Unable to open file!");
        $filesize=filesize('D:\MATLAB\RetinalDetectionPFE-master\21_f1_mat.txt');
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $nbligne=sizeof($vector);
        $nbligne=$nbligne-1;
        
        $v2=array();
        for ($i=0; $i <$nbligne ; $i++) { 
            $ligneone=explode(",", $vector[$i]); 
            $ligneone=array_filter($ligneone);
            $v2[$i]=array_sum($ligneone)/sizeof($ligneone);
        }
        fclose($myfile);
        //Calcule Distance
        $sum=0;
        for ($i=0; $i <$nbligne ; $i++) { 
            $sum=$sum+pow(($v1[$i]-$v2[$i]),2);
        }
        $sum=sqrt($sum);
        dd($sum);  
    }

    function enregistrerAllDataSet(){
        $fileracine="";
        $filepath="";
        $dtSet=array();
        $imagePath="";
        $filmat="DataSet/Means/";
        for ($i=1; $i <41 ; $i++) { 
            $filmat="DataSet/Means/";
            $dtSet=[];
            $imagePath="";
            $filepath="";
            $fileracine="";
            if ($i<10){
                $fileracine="0".$i;
                $imagePath="0";
                $filmat=$filmat."0";
            }else{
                $fileracine="".$i;
            }
            $filmat=$filmat.$i."t_mean.txt";
            if ($i<21){
                
                $imagePath=$imagePath.$i."_test.tif";
            }else{
                $imagePath=$imagePath.$i."_training.tif";
            }
            
            $imagePath="DataSet/Images/".$imagePath;
            $dtSet=[
                'titre'=>"image number ".$i,
                'image'=>$imagePath,
                'GroundTruth'=>'',
                'drive_remarque'=>'',
                'doctor_Remarque'=>'',
                'nbLigne'=>584,
                'nbColonne'=>565,
                'Features_oneFile'=>'',
                'Features_TwoFile'=>'',
                'Features_ThreeFile'=>'',
                'Features_FourFile'=>'',
                'Features_FiveFile'=>'',
                'Features_SixFile'=>'',
                'Features_SevenFile'=>'',
                'FileMatPhath'=>$filmat
            ];
            
            for ($j=1; $j <8 ; $j++) { 
                $filepath=$fileracine."_f".$j."_mat.txt";
                $filepath="DataSet/Features/".$filepath;
                if ($j==1){
                    $dtSet['Features_oneFile']=$filepath;
                }
                if ($j==2){
                    $dtSet['Features_TwoFile']=$filepath;
                }
                if ($j==3){
                    $dtSet['Features_ThreeFile']=$filepath;
                }
                if ($j==4){
                    $dtSet['Features_FourFile']=$filepath;
                }
                if ($j==5){
                    $dtSet['Features_FiveFile']=$filepath;
                }
                if ($j==6){
                    $dtSet['Features_SixFile']=$filepath;
                }
                if ($j==7){
                    $dtSet['Features_SevenFile']=$filepath;
                }
                
            }
            $dataSet=new DataSetImage($dtSet);
            $dataSet->save();
        }
        dd("NoError");
    }
    function CbirResults($id){
        $result=[];
        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return $result;
        }
        $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
        if ($imagetrate == null){
            return $result;
        }
        $filepath=asset($imagetrate->FileMatPhath);
        $filepath="C:\wamp64\www\Retina\public\\".$imagetrate->FileMatPhath;
        if (file_exists($filepath) == false){
            return $result;   
        }
        $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=','E')->orderBy('ordre')->get();
        if (sizeof($oldResult) > 0){
            for ($i=0; $i <sizeof($oldResult) ; $i++) { 
                $dataset=DataSetImage::where('id','=',$oldResult[$i]->DataSet_id)->first();
                $oldResult[$i]['Image']=$dataset->image;
                $oldResult[$i]['drive_remarque']=$dataset->drive_remarque;
                $oldResult[$i]['doctor_Remarque']=$dataset->doctor_Remarque;
                $oldResult[$i]['GroundTruth']=$dataset->GroundTruth;
            }
            return $oldResult;
        }
        $alldataSet=DataSetImage::get();
        $myfile = fopen($filepath, "r");
        $filesize=filesize($filepath);
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $vector=explode(",", $vector[0]);
        fclose($myfile);
        $vectortier=[];
        $vectorResult=[];
        for ($i=0; $i <sizeof($alldataSet) ; $i++) { 
            //$filepath=asset($alldataSet[$i]->FileMatPhath);
            $filepath="C:\wamp64\www\Retina\public\\".$alldataSet[$i]->FileMatPhath;
            if (file_exists($filepath)){
                $myfile = fopen($filepath, "r");
                $filesize=filesize($filepath);
                $fileContent=fread($myfile,$filesize);
                $vectorEncour=explode("\n", $fileContent);
                $vectorEncour=explode(",", $vectorEncour[0]);
                fclose($myfile);
                $sum=0;
                for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                    $sum=$sum+pow(($vectorEncour[$j]-$vector[$j]),2);
                }
                $sum=sqrt($sum);
                $vectortier[$i]=$sum;
                $vectorResult[$i]['distance']=$sum;
                $vectorResult[$i]['datasetID']=$alldataSet[$i]->id;
                $vectorResult[$i]['Image']=$alldataSet[$i]->image;
                $vectorResult[$i]['drive_remarque']=$alldataSet[$i]->drive_remarque;
                $vectorResult[$i]['doctor_Remarque']=$alldataSet[$i]->doctor_Remarque;
                $vectorResult[$i]['GroundTruth']=$alldataSet[$i]->GroundTruth;
            }
        }
        $vectorResult=$this->array_sort($vectorResult,'distance',SORT_ASC);
        $i=0;
        $resultadd=[];
        $finalVectore=array();

        foreach ($vectorResult as $key => $value) {
           // dd($value);
            $finalVectore[$i]['ordre']=$i+1;
            $finalVectore[$i]['Image']=$value['Image'];
            $finalVectore[$i]['distance']=$value['distance'];
            $finalVectore[$i]['drive_remarque']=$value['drive_remarque'];
            $finalVectore[$i]['doctor_Remarque']=$value['doctor_Remarque'];
            $finalVectore[$i]['GroundTruth']=$value['GroundTruth'];
            $resultadd['imageTreated_id']=$imagetrate->id;
            $resultadd['DataSet_id']=$value['datasetID'];
            $resultadd['ordre']=$i+1;
            $resultadd['distance']=$value['distance'];
            $resultadd['algo']='E';
            $enregist=new CbirResult($resultadd);
            $enregist->save();
            $i++;
        }
        return $finalVectore;
    }


    function CbirResultsCos($id){
        $result=[];
        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return $result;
        }
        $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
        if ($imagetrate == null){
            return $result;
        }
        $filepath=asset($imagetrate->FileMatPhath);
        $filepath="C:\wamp64\www\Retina\public\\".$imagetrate->FileMatPhath;
        if (file_exists($filepath) == false){
            return $result;   
        }
        $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=','C')->orderBy('ordre')->get();
        if (sizeof($oldResult) > 0){
            for ($i=0; $i <sizeof($oldResult) ; $i++) { 
                $dataset=DataSetImage::where('id','=',$oldResult[$i]->DataSet_id)->first();
                $oldResult[$i]['Image']=$dataset->image;
                $oldResult[$i]['drive_remarque']=$dataset->drive_remarque;
                $oldResult[$i]['doctor_Remarque']=$dataset->doctor_Remarque;
                $oldResult[$i]['GroundTruth']=$dataset->GroundTruth;
            }
            return $oldResult;
        }
        $alldataSet=DataSetImage::get();
        $myfile = fopen($filepath, "r");
        $filesize=filesize($filepath);
        $fileContent=fread($myfile,$filesize);
        $vector=explode("\n", $fileContent);
        $vector=explode(",", $vector[0]);
        fclose($myfile);
        $vectortier=[];
        $vectorResult=[];
        for ($i=0; $i <sizeof($alldataSet) ; $i++) { 
            //$filepath=asset($alldataSet[$i]->FileMatPhath);
            $filepath="C:\wamp64\www\Retina\public\\".$alldataSet[$i]->FileMatPhath;
            if (file_exists($filepath)){
                $myfile = fopen($filepath, "r");
                $filesize=filesize($filepath);
                $fileContent=fread($myfile,$filesize);
                $vectorEncour=explode("\n", $fileContent);
                $vectorEncour=explode(",", $vectorEncour[0]);
                fclose($myfile);
                $sum=0;
                $kimA=0;
                $kimB=0;
                for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                    $sum=$sum+($vectorEncour[$j]*$vector[$j]);
                    $kimA=$kimA+pow($vector[$j],2);
                    $kimB=$kimB+pow($vectorEncour[$j],2);
                }
                $kimA=sqrt($kimA);
                $kimB=sqrt($kimB);
                $kimB=$kimB*$kimA;
                $sum=$sum/$kimB;
                $vectortier[$i]=$sum;
                $vectorResult[$i]['distance']=$sum;
                $vectorResult[$i]['datasetID']=$alldataSet[$i]->id;
                $vectorResult[$i]['Image']=$alldataSet[$i]->image;
                $vectorResult[$i]['drive_remarque']=$alldataSet[$i]->drive_remarque;
                $vectorResult[$i]['doctor_Remarque']=$alldataSet[$i]->doctor_Remarque;
                $vectorResult[$i]['GroundTruth']=$alldataSet[$i]->GroundTruth;
            }
        }
        $vectorResult=$this->array_sort($vectorResult,'distance',SORT_DESC);
        $i=0;
        $resultadd=[];
        $finalVectore=array();

        foreach ($vectorResult as $key => $value) {
           // dd($value);
            $finalVectore[$i]['ordre']=$i+1;
            $finalVectore[$i]['Image']=$value['Image'];
            $finalVectore[$i]['distance']=$value['distance'];
            $finalVectore[$i]['drive_remarque']=$value['drive_remarque'];
            $finalVectore[$i]['doctor_Remarque']=$value['doctor_Remarque'];
            $finalVectore[$i]['GroundTruth']=$value['GroundTruth'];
            $resultadd['imageTreated_id']=$imagetrate->id;
            $resultadd['DataSet_id']=$value['datasetID'];
            $resultadd['ordre']=$i+1;
            $resultadd['distance']=$value['distance'];
            $resultadd['algo']='C';
            $enregist=new CbirResult($resultadd);
            $enregist->save();
            $i++;
        }
        return $finalVectore;
    }

    function create($id){
        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return redirect('Clients');
        }
        $colorImage="";
    //Supervised GLCM 
        $filepath="C:\wamp64\www\Retina\public\\".$imgsour->glsmSupervised;
        if (file_exists($filepath) == false){

        }else{
             $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
            if ($imagetrate == null){
                //return $result;
            }else{
                $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=','SGE')->first();
                if ($oldResult ==  null){
                    $alldataSet=DataSetImage::get();
                    $myfile = fopen($filepath, "r");
                    $filesize=filesize($filepath);
                    $fileContent=fread($myfile,$filesize);
                    $vector=explode("\n", $fileContent);
                    $vector=explode(",", $vector[0]);
                    fclose($myfile);
                    $vectorResult=[];
                    $vectorResultcosine=[];
                    for ($i=0; $i <sizeof($alldataSet) ; $i++) {
                        $filepath="C:\wamp64\www\Retina\public\\".$alldataSet[$i]->Features_ThreeFile;
                        if (file_exists($filepath)){
                            $myfile = fopen($filepath, "r");
                            $filesize=filesize($filepath);
                            $fileContent=fread($myfile,$filesize);
                            $vectorEncour=explode("\n", $fileContent);
                            $vectorEncour=explode(",", $vectorEncour[0]);
                            fclose($myfile);
                            //Euclidean
                            $sum=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sum=$sum+pow(($vectorEncour[$j]-$vector[$j]),2);
                            }
                            $sum=sqrt($sum);
                            $vectorResult[$i]['distance']=$sum;
                            $vectorResult[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResult[$i]['color']=$alldataSet[$i]->color;
                            //Cosine
                            $sumCos=0;
                            $kimA=0;
                            $kimB=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sumCos=$sumCos+($vectorEncour[$j]*$vector[$j]);
                                $kimA=$kimA+pow($vector[$j],2);
                                $kimB=$kimB+pow($vectorEncour[$j],2);
                            }
                            $kimA=sqrt($kimA);
                            $kimB=sqrt($kimB);
                            $kimB=$kimB*$kimA;
                            $sumCos=$sumCos/$kimB;
                            $vectorResultcosine[$i]['distance']=$sumCos;
                            $vectorResultcosine[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResultcosine[$i]['color']=$alldataSet[$i]->color;
                        }
                    }
                    $vectorResult=$this->array_sort($vectorResult,'distance',SORT_ASC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResult as $key => $value) {
                        if ($i==0){
                            $colorImage=$value['color'];
                        }
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='SGE';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                    $vectorResultcosine=$this->array_sort($vectorResultcosine,'distance',SORT_DESC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResultcosine as $key => $value) {
                        if ($i==0){
                            $colorImage=$value['color'];
                        }
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='SGC';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                }
            } 
        }
    // Supervised HOG
    $filepath="C:\wamp64\www\Retina\public\\".$imgsour->HogSupervised;
        if (file_exists($filepath) == false){

        }else{
             $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
            if ($imagetrate == null){
                //return $result;
            }else{
                $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=','SHE')->first();
                if ($oldResult ==  null){
                    $alldataSet=DataSetImage::get();
                    $myfile = fopen($filepath, "r");
                    $filesize=filesize($filepath);
                    $fileContent=fread($myfile,$filesize);
                    $vector=explode("\n", $fileContent);
                    $vector=explode(",", $vector[0]);
                    fclose($myfile);
                    $vectorResult=[];
                    $vectorResultcosine=[];
                    for ($i=0; $i <sizeof($alldataSet) ; $i++) {
                        $filepath="C:\wamp64\www\Retina\public\\".$alldataSet[$i]->Features_FourFile;
                        if (file_exists($filepath)){
                            $myfile = fopen($filepath, "r");
                            $filesize=filesize($filepath);
                            $fileContent=fread($myfile,$filesize);
                            $vectorEncour=explode("\n", $fileContent);
                            $vectorEncour=explode(",", $vectorEncour[0]);
                            fclose($myfile);
                            //Euclidean
                            $sum=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sum=$sum+pow(($vectorEncour[$j]-$vector[$j]),2);
                            }
                            $sum=sqrt($sum);
                            $vectorResult[$i]['distance']=$sum;
                            $vectorResult[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResult[$i]['color']=$alldataSet[$i]->color;
                            //Cosine
                            $sumCos=0;
                            $kimA=0;
                            $kimB=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sumCos=$sumCos+($vectorEncour[$j]*$vector[$j]);
                                $kimA=$kimA+pow($vector[$j],2);
                                $kimB=$kimB+pow($vectorEncour[$j],2);
                            }
                            $kimA=sqrt($kimA);
                            $kimB=sqrt($kimB);
                            $kimB=$kimB*$kimA;
                            $sumCos=$sumCos/$kimB;
                            $vectorResultcosine[$i]['distance']=$sumCos;
                            $vectorResultcosine[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResultcosine[$i]['color']=$alldataSet[$i]->color;
                        }
                    }
                    $vectorResult=$this->array_sort($vectorResult,'distance',SORT_ASC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResult as $key => $value) {
                        if ($i==0){
                            $colorImage=$value['color'];
                        }
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='SHE';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                    $vectorResultcosine=$this->array_sort($vectorResultcosine,'distance',SORT_DESC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResultcosine as $key => $value) {
                        if ($i==0){
                            $colorImage=$value['color'];
                        }
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='SHC';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                }
            } 
        }
    //end
    //Proposed GLCM
    $filepath="C:\wamp64\www\Retina\public\\".$imgsour->glsmProposed;
        if (file_exists($filepath) == false){

        }else{
             $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
            if ($imagetrate == null){
                //return $result;
            }else{
                $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=','PGE')->first();
                if ($oldResult ==  null){
                    $alldataSet=DataSetImage::get();
                    $myfile = fopen($filepath, "r");
                    $filesize=filesize($filepath);
                    $fileContent=fread($myfile,$filesize);
                    $vector=explode("\n", $fileContent);
                    $vector=explode(",", $vector[0]);
                    fclose($myfile);
                    $vectorResult=[];
                    $vectorResultcosine=[];
                    for ($i=0; $i <sizeof($alldataSet) ; $i++) {
                        $filepath="C:\wamp64\www\Retina\public\\".$alldataSet[$i]->Features_oneFile;
                        if (file_exists($filepath)){
                            $myfile = fopen($filepath, "r");
                            $filesize=filesize($filepath);
                            $fileContent=fread($myfile,$filesize);
                            $vectorEncour=explode("\n", $fileContent);
                            $vectorEncour=explode(",", $vectorEncour[0]);
                            fclose($myfile);
                            //Euclidean
                            $sum=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sum=$sum+pow(($vectorEncour[$j]-$vector[$j]),2);
                            }
                            $sum=sqrt($sum);
                            $vectorResult[$i]['distance']=$sum;
                            $vectorResult[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResult[$i]['color']=$alldataSet[$i]->color;
                            //Cosine
                            $sumCos=0;
                            $kimA=0;
                            $kimB=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sumCos=$sumCos+($vectorEncour[$j]*$vector[$j]);
                                $kimA=$kimA+pow($vector[$j],2);
                                $kimB=$kimB+pow($vectorEncour[$j],2);
                            }
                            $kimA=sqrt($kimA);
                            $kimB=sqrt($kimB);
                            $kimB=$kimB*$kimA;
                            $sumCos=$sumCos/$kimB;
                            $vectorResultcosine[$i]['distance']=$sumCos;
                            $vectorResultcosine[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResultcosine[$i]['color']=$alldataSet[$i]->color;
                        }
                    }
                    $vectorResult=$this->array_sort($vectorResult,'distance',SORT_ASC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResult as $key => $value) {
                        if ($i==0){
                            $colorImage=$value['color'];
                        }
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='PGE';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                    $vectorResultcosine=$this->array_sort($vectorResultcosine,'distance',SORT_DESC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResultcosine as $key => $value) {
                        
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='PGC';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                }
            } 
        }    
    //End
    //Proposed HOG
    $filepath="C:\wamp64\www\Retina\public\\".$imgsour->HogProposed;
        if (file_exists($filepath) == false){

        }else{
             $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
            if ($imagetrate == null){
                //return $result;
            }else{
                $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=','PHE')->first();
                if ($oldResult ==  null){
                    $alldataSet=DataSetImage::get();
                    $myfile = fopen($filepath, "r");
                    $filesize=filesize($filepath);
                    $fileContent=fread($myfile,$filesize);
                    $vector=explode("\n", $fileContent);
                    $vector=explode(",", $vector[0]);
                    fclose($myfile);
                    $vectorResult=[];
                    $vectorResultcosine=[];
                    for ($i=0; $i <sizeof($alldataSet) ; $i++) {
                        $filepath="C:\wamp64\www\Retina\public\\".$alldataSet[$i]->Features_TwoFile;
                        if (file_exists($filepath)){
                            $myfile = fopen($filepath, "r");
                            $filesize=filesize($filepath);
                            $fileContent=fread($myfile,$filesize);
                            $vectorEncour=explode("\n", $fileContent);
                            $vectorEncour=explode(",", $vectorEncour[0]);
                            fclose($myfile);
                            //Euclidean
                            $sum=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sum=$sum+pow(($vectorEncour[$j]-$vector[$j]),2);
                            }
                            $sum=sqrt($sum);
                            $vectorResult[$i]['distance']=$sum;
                            $vectorResult[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResult[$i]['color']=$alldataSet[$i]->color;
                            //Cosine
                            $sumCos=0;
                            $kimA=0;
                            $kimB=0;
                            for ($j=0; $j <sizeof($vectorEncour) ; $j++) { 
                                $sumCos=$sumCos+($vectorEncour[$j]*$vector[$j]);
                                $kimA=$kimA+pow($vector[$j],2);
                                $kimB=$kimB+pow($vectorEncour[$j],2);
                            }
                            $kimA=sqrt($kimA);
                            $kimB=sqrt($kimB);
                            $kimB=$kimB*$kimA;
                            $sumCos=$sumCos/$kimB;
                            $vectorResultcosine[$i]['distance']=$sumCos;
                            $vectorResultcosine[$i]['datasetID']=$alldataSet[$i]->id;
                            $vectorResultcosine[$i]['color']=$alldataSet[$i]->color;
                        }
                    }
                    $vectorResult=$this->array_sort($vectorResult,'distance',SORT_ASC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResult as $key => $value) {
                        
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='PHE';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                    $vectorResultcosine=$this->array_sort($vectorResultcosine,'distance',SORT_DESC);
                    $i=0;
                    $resultadd=[];
                    foreach ($vectorResultcosine as $key => $value) {
                        
                        $resultadd['imageTreated_id']=$imagetrate->id;
                        $resultadd['DataSet_id']=$value['datasetID'];
                        $resultadd['ordre']=$i+1;
                        $resultadd['distance']=$value['distance'];
                        $resultadd['algo']='PHC';
                        $resultadd['color']=$value['color'];
                        $enregist=new CbirResult($resultadd);
                        $enregist->save();
                        $i++;
                    }
                }
            } 
        }
    if ($colorImage==1){
        $imgsour['color']="#F01D1D";
    }else{
        $imgsour['color']="#2AD593";
    }
    //End    
    return view('Client.CbirViewResult',array('ImageSource'=>$imgsour));
}

    function array_sort($array, $on, $order=SORT_ASC)
    {
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}



function ResultaCbir($id,$Algo,$nb,$type){
    $result=[];
    $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return $result;
        }
    $imagetrate=imagetraited::where('ImageSource_id','=',$imgsour->id)->first();
    if ($imagetrate == null){
        return $result;
    }
    $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=',$Algo)->orderBy('ordre')->get();
    $imageTy="";
    $colo="";
    for ($i=0; $i <$nb ; $i++) { 
        $datase=DataSetImage::where('id','=',$oldResult[$i]->DataSet_id)->first();
        if ($type==1){
            $imageTy=$datase->image;
        }else{
            if($Algo[0]=='P'){
              $imageTy=$datase->GroundTruthProposed;  
            }else{
                $imageTy=$datase->GroundTruth;
            }
        }
        if ($oldResult[$i]->color==1){
            $colo="#F01D1D";
        }else{
            $colo="#2AD593";
        }
        $result[$i]=[
            'color'=>$colo,
            'id'=>$oldResult[$i]->id,
            'title'=>$datase->titre,
            'distance'=>$oldResult[$i]->distance,
            'image'=>$imageTy
        ];
    }
    return $result;
}
function GraphPGC($id,$Algo){
    $result=[];
    $imagetrate=imagetraited::where('id','=',$id)->first();
    if ($imagetrate == null){
        return $result;
    }
    $oldResult=CbirResult::where('imageTreated_id','=',$imagetrate->id)->where('algo','=',$Algo)->orderBy('ordre')->get();
    $colorfris=0;
    $Casfavo=1;
    $percent=0;
    $result[0]=[
        'x'=>1,
        'y'=>100,
    ];
    $j=1;

    for ($i=0; $i < 20; $i++) { 
        if ($i==0){
            $colorfris=$oldResult[$i]->color;
        }else{
            if(($i+1)%5==0){
                $percent=$Casfavo/($i+1);
                $percent=$percent*100;
                $result[$j]=[
                    'x'=>$i+1,
                    'y'=>$percent
                ];
                $j++;
            }
            if($oldResult[$i]->color == $colorfris){
                $Casfavo++;
            }
        }
    }
    return $result;
}

}

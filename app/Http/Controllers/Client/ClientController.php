<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Client;
use App\ImageSource;
use App\imagetraited;
use App\Step;

class ClientController extends Controller
{
    function Client(){
        $id = Auth::id();
        if ($id == null){
        return redirect('Login');   
        }
        return view('Client.Liste');
    }
    function Graph($id){
        
        $imageTraite=imagetraited::where('ImageSource_id','=',$id)->first();
        return view('image.chart',array('ImageTraited'=>$imageTraite));
    }
    function ListeClient(){
    	$id = Auth::id();
        
        if ($id == null){
        return redirect('Login');   
        }
   		$client=Client::where('medcin_id','=',$id)->get();
        return $client;
    }
    function add(){
        $id = Auth::id();
        if ($id == null){
        return redirect('Login');   
        }
        return view('Client.add');
    }
    function Store(Request $request){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }
        $credentials= $request->only('nom','prenom','email','mobile','telephone','Remarque');
        $rules = [
            'nom' => 'required|string|max:60',
            'prenom' => 'required|string|max:60',
            'email' => 'required|string|max:60',
            'mobile' => 'required|string|max:60',
            'telephone' => 'required|string|max:60',
            'Remarque' => 'required|string|max:5000'
        ];
        $validator = \Validator::make($credentials,$rules);
        $result=array();
        if ($validator->fails()) {
            $result['error']=$validator->messages();
            return response()->json($result, 200);
        }
        $credentials['medcin_id']=$idMedcin;
        $credentials['etat']=1;
        $cl = new Client($credentials);
        $cl->save();
        return $cl;

    }
    function ListeImage($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }

        $client=Client::where('id','=',$id)->first();
        if ($client == null){
         return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }
        return view('Imagesource.ImagePerClient',array('client'=>$client));
    }


    function ImagePerClient($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }

        $client=Client::where('id','=',$id)->first();
        if ($client == null){
        return redirect('Clients');   
        }

        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }
        $imag=ImageSource::where('Client_id','=',$id)->get();
        for ($i=0; $i <sizeof($imag) ; $i++) {
            $traited=imagetraited::where('ImageSource_id','=',$imag[$i]['id'])->first();
            if ($traited != null) {
                $imag[$i]['FirstTraited']=$traited->image;
                $imag[$i]['filetxt']=$traited->FileMatPhath;
            }else{
                $imag[$i]['FirstTraited']="";
                $imag[$i]['filetxt']="";
            }
        }
        return $imag;
    }


    function similaireImage($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }

        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return redirect('Clients');
        }

        $client=Client::where('id','=',$imgsour->Client_id)->first();
        if ($client == null){
        return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }    
        return view('Imagesource.CbirResult',array('ImageSource'=>$imgsour));
    }

    function similaireImageCos($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }

        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return redirect('Clients');
        }

        $client=Client::where('id','=',$imgsour->Client_id)->first();
        if ($client == null){
        return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }    
        return view('Imagesource.CbirResultCos',array('ImageSource'=>$imgsour));
    }


    function SegementationSteps($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }

        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return redirect('Clients');
        }

        $client=Client::where('id','=',$imgsour->Client_id)->first();
        if ($client == null){
        return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }    
        return view('Imagesource.StepsOfSegmentation',array('ImageSource'=>$imgsour));
    }


    function SegementationStepsList($id){
        $idMedcin = Auth::id();
        if ($idMedcin == null){
        return redirect('Login');   
        }

        $imgsour=ImageSource::where('id','=',$id)->first();
        if($imgsour == null){
            return redirect('Clients');
        }

        $client=Client::where('id','=',$imgsour->Client_id)->first();
        if ($client == null){
        return redirect('Clients');   
        }
        if ($client->medcin_id != $idMedcin ){
         return redirect('Clients');   
        }
        $imagetraited=  imagetraited::where('ImageSource_id','=',$id)->first();
        $Steps=Step::where('imageTreated_id','=',$imagetraited->id)->get();  
        return $Steps;
    }
       
}

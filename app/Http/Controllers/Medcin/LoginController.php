<?php

namespace App\Http\Controllers\Medcin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        //validate
        $rules = ['email' => 'required|email|exists:Medcins','password' => 'required|min:6|max:25',];
        $validator = \Validator::make($credentials,$rules);
        $result=array();
        // validate

        if ($validator->fails()) {
            $result['error']=$validator->messages();
            return response()->json($result, 200);
        }
        
        //check etat
        $agenceuser=\App\Medcin::where('email',$credentials['email'])->first();
        
        //try authentication

        if (\Auth::attempt($credentials)) {
        // Authentication passed...
            return response()->json($result,200);
        }else{// Authentication errors...
            
            $result['error']=['password' => [trans('strings.errPassword')]];
            return response()->json($result,200);
        }
    }
    function logout(){
        \Auth::logout();
        return redirect('Login');
    }
}

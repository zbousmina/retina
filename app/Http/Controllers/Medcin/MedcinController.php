<?php

namespace App\Http\Controllers\Medcin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Medcin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class MedcinController extends Controller
{
    function login(){
        $id = Auth::id();
        
        if ($id == null){
        return view('Medcin.login');    
        }
    	return redirect('Clients');
    }
    function create(){
    	return view('Medcin.create');
    }
    function store(Request $request){
        $credentials = $request->only('nom','prenom','email','mobile','password','passwordConfirmation');
        //validate
        $rules = [
            'nom' => 'required|string|max:64',
            'prenom' => 'required|string|max:64',
            'email' => 'required|email|unique:Medcins',
            'mobile' => 'required|numeric',
            'password' => 'required|string|max:64|min:6',
        ];
        $validator = \Validator::make($credentials,$rules);
        $result=array();
        // validate
        if ($validator->fails()) {
            $result['error']=$validator->messages();
            return response()->json($result, 200);
        }
        $credentials['etat']=1;
        $credentials['password']=Hash::make($credentials['password']);
        $agence=Medcin::create($credentials);
        return $result;
    }
}

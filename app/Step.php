<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $fillable = [
    	'titre','image','imageTreated_id'
    ];
    public function imagetraited()
    {
        return $this->belongsTo('App\imagetraited','imageTreated_id');
    }
}

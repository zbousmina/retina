<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
    	'medcin_id','nom','prenom','email','telephone','mobile','etat','Remarque'
    ];
    public function medcin()
    {
        return $this->belongsTo('App\Medcin','medcin_id');
    }
    public function imagesource()
    {
        return $this->hasMany('App\ImageSource');
    }
}

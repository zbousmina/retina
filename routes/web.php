<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','Medcin\MedcinController@login')->name('Login');

Route::get('/traiter','Matlab\PhotoController@formView');
//Route::post('/traiter','Matlab\PhotoController@Traiterimage');
//Route::post('/traiter','Matlab\PhotoController@GetFeatureExtractionSupervised');

/*Auth::routes();*/
Route::get('/Login','Medcin\MedcinController@login');
Route::post('/Login','Medcin\LoginController@login');
Route::get('/Agences/Inscription','Medcin\MedcinController@create');
Route::post('/Agences/Inscription','Medcin\MedcinController@store');
Route::get('/Client/Add','Client\ClientController@add');
Route::post('/Client/Add','Client\ClientController@Store');
Route::get('/Clients','Client\ClientController@Client');
Route::get('/api/MyClients','Client\ClientController@ListeClient');
Route::get('/ImageClient/{id}','Client\ClientController@ListeImage');
Route::get('/api/ImageClient/{id}','Client\ClientController@ImagePerClient');

Route::get('/Image/Add/{id}','Matlab\PhotoController@formView');
Route::post('/Image/Add/{id}','Matlab\PhotoController@Traiterimage');

Route::get('/Image/AddSupervised/{id}','Matlab\PhotoController@formViewSupevised');
Route::post('/Image/AddSupervised/{id}','Matlab\PhotoController@GetFeatureExtractionSupervised');

Route::get('/Graph/{id}','Client\ClientController@Graph');


Route::get('/similaireCase/{id}','Client\ClientController@similaireImage');
Route::get('/api/CbirResult/{id}','Matlab\PhotoController@CbirResults');



Route::get('/api/GraphPGC/{id}/{Algo}','Matlab\PhotoController@GraphPGC');

Route::get('/similaireCaseCos/{id}','Client\ClientController@similaireImageCos');
Route::get('/api/CbirResultCos/{id}','Matlab\PhotoController@CbirResultsCos');


Route::get('/CbirResultModel/{id}','Matlab\PhotoController@create');
Route::get('/api/CbirResult/{id}/{Algo}/{nb}/{type}','Matlab\PhotoController@ResultaCbir');


Route::get('/SegementationSteps/{id}','Client\ClientController@SegementationSteps');
Route::get('/api/SegementationSteps/{id}','Client\ClientController@SegementationStepsList');

Route::get('/filetxtTest','Matlab\PhotoController@handleTxtFileTest');

Route::get('/Logout','Medcin\LoginController@logout');

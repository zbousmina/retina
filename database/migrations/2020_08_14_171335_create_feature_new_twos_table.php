<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureNewTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_new_twos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('imageTreated_id')->nullable();
            $table->float('valeur',12,12)->nullable();
            $table->integer('ligne')->nullable();
            $table->integer('colonne')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_new_twos');
    }
}

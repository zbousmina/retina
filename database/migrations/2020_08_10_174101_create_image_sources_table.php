<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('Client_id')->nullable();
            $table->string('image',300)->nullable();
            $table->string('Remarque',5000)->nullable();
            $table->string('finaleSegementeProposed',300)->nullable();
            $table->string('finaleSegementeSupervised',300)->nullable();
            $table->string('glsmSupervised',600)->nullable();
            $table->string('HogSupervised',600)->nullable();
            $table->string('glsmProposed',600)->nullable();
            $table->string('HogProposed',600)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_sources');
    }
}

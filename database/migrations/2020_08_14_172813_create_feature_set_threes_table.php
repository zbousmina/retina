<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureSetThreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_set_threes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('DataSet_id')->nullable();
            $table->float('valeur',12,12)->nullable();
            $table->integer('ligne')->nullable();
            $table->integer('colonne')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_set_threes');
    }
}

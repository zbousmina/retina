<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCbirResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cbir_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('imageTreated_id')->nullable();
            $table->bigInteger('DataSet_id')->nullable();
            $table->integer('ordre')->nullable();
            $table->string('distance')->nullable();
            $table->string('algo')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cbir_results');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagetraitedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagetraiteds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ImageSource_id')->nullable();
            $table->string('image',300)->nullable();
            $table->string('Remarque',5000)->nullable();
            $table->integer('nbLigne')->nullable();
            $table->integer('nbColonne')->nullable();
            $table->bigInteger('Algorithm_id')->nullable();
            $table->string('Features_oneFile',600)->nullable();
            $table->string('Features_TwoFile',600)->nullable();
            $table->string('Features_ThreeFile',600)->nullable();
            $table->string('Features_FourFile',600)->nullable();
            $table->string('Features_FiveFile',600)->nullable();
            $table->string('Features_SixFile',600)->nullable();
            $table->string('Features_SevenFile',600)->nullable();
            $table->string('FileMatPhath',600)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagetraiteds');
    }
}

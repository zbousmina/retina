@extends('layouts.main')
@section('title',__('patient list'))
@section('app')
<div class="container-fluid">
	<div class="pull-left">
		<h3 style="display: inline;">Similaire case to the Image using euclidean distance  </h3>
		<img src="{{ URL::asset('/images'.'/'.$ImageSource->image) }}" height="75" width="75"></img>
	</div>
	<?php
				    $idImage=$ImageSource->id;
				 ?>
	<table id="mainTable" class="display nowrap" data-page-length="5">
	    <thead>
	        <tr>
	        	<th>Ranking</th>
	            <th>Image</th>
	            <th>Image</th>
	            <th>GroundTruth</th>
	            <th>GroundTruth</th>
	            <th>Distance</th>
	            <th>Data Note</th>
	            <th>Doctor Note</th>
	        </tr>
	    </thead>
	</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var mainTable=0;
	function initTable(){
			mainTable=$('#mainTable').DataTable({
				// "scrollX": true,
				// "autoWidth":false,
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				"language": {
		            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+
		            (('{{ app()->getLocale() }}'=='fr') ? 'French' : ('{{ app()->getLocale() }}'=='en') ? 'English' : 'Arabic')
		            +".json"
		        },
				ajax: {
			    	url: '{{ URL::to("/api/CbirResult"."/".$idImage) }}',
			    	dataSrc: ''
			    },
			    columns:[
			    	{data:'ordre'},
			    	{
			    		data:'Image',
			    		render:function(data,type,row){
			    			if(data=='') return '';
					        return `<a href="{{ URL::asset(".") }}/`+data+`" target="_blank">Click to download the Image</a>`;
					    }
			    	},
			    	{
			    		data:'Image',
			    		render:function(data,type,row){
			    			if(data=='') return ''; 
					        return '<img src="{{ URL::asset(".") }}/'+data+'" alt="image '+row.nomAgence+'" height="50" width="50"></img>';
					    }
			    	},
			    	{
			    		data:'GroundTruth',
			    		render:function(data,type,row){
			    			if(data=='') return '';
					        return `<a href="{{ URL::asset(".") }}/`+data+`" target="_blank">Click to download the Image</a>`;
					    }
			    	},
			    	{
			    		data:'GroundTruth',
			    		render:function(data,type,row){
			    			if(data=='') return ''; 
					        return '<img src="{{ URL::asset(".") }}/'+data+'" alt="image '+row.nomAgence+'" height="50" width="50"></img>';
					    }
			    	},
			    	{data:'distance'},
			    	{
			    		data:'drive_remarque',
			    		render:function(data,type,row){
			    			var s='';
			    			for(var i=0;i<data.length;i++){
			    				if(i%40==39) s+='<br>';
			    				s+=data[i];
			    			}
			    			return s;
			    		}
			    	},
			    	{
			    		data:'doctor_Remarque',
			    		render:function(data,type,row){
			    			var s='';
			    			for(var i=0;i<data.length;i++){
			    				if(i%40==39) s+='<br>';
			    				s+=data[i];
			    			}
			    			return s;
			    		}
			    	}
			    ]
			});
		}
		 $(document).ready(function(){
		 	
			initTable();
			
		});

	</script>
@endsection
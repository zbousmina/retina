@extends('layouts.main')
@section('title',__('patient list'))
@section('app')
<div class="container-fluid">
	<div class="pull-left">
		<h3 style="display: inline;">{{ $client->nom.'  '.$client->prenom }}   </h3>
		<?php
				    $idcl=$client->id;
				 ?>
		<a href="{{ URL::to('Image/AddSupervised'.'/'.$idcl) }}" class="btn btn-rose btn-raised btn-round">
			Add an Image
		</a>
	</div>
	<table id="mainTable" class="display nowrap">
	    <thead>
	        <tr>
	        	<th>Date</th>
	            <th>Image</th>
	            <th>Image</th>
	            <th>Note</th>
	            <th>Result</th>
	            <th>Result</th>
	            <th>action</th>
	        </tr>
	    </thead>
	</table>
</div>
@include('layouts.modal_Cbir')
@endsection
@section('scripts')
<script type="text/javascript">
	var mainTable=0;
	function initTable(){
			mainTable=$('#mainTable').DataTable({
				// "scrollX": true,
				// "autoWidth":false,
				
				"language": {
		            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+
		            (('{{ app()->getLocale() }}'=='fr') ? 'French' : ('{{ app()->getLocale() }}'=='en') ? 'English' : 'Arabic')
		            +".json"
		        },
				ajax: {
			    	url: '{{ URL::to("/api/ImageClient"."/".$idcl) }}',
			    	dataSrc: ''
			    },
			    columns:[
			    	{data:'created_at'},
			    	{
			    		data:'image',
			    		render:function(data,type,row){
			    			if(data=='') return '';
					        return `<a href="{{ URL::asset("./images") }}/`+data+`" target="_blank">Click to download the Image</a>`;
					    }
			    	},
			    	{
			    		data:'image',
			    		render:function(data,type,row){
			    			if(data=='') return ''; 
					        return '<img src="{{ URL::asset("./images") }}/'+data+'" alt="image '+row.nomAgence+'" height="50" width="50"></img>';
					    }
			    	},
			    	{data:'Remarque'},
			    	{
			    		data:'FirstTraited',
			    		render:function(data,type,row){
			    			if(data=='') return 'Not traited yet';
					        return `<a href="{{ URL::asset("./images") }}/`+data+`" target="_blank">Click to download the Image</a>`;
					    }
			    	},
			    	{
			    		data:'FirstTraited',
			    		render:function(data,type,row){
			    			if(data=='') return 'Not traited yet'; 
					        return '<img src="{{ URL::asset("./images") }}/'+data+'" alt="image '+row.nomAgence+'" height="50" width="50"></img>';
					    }
			    	},
			    	{
			    		data:'id',
			    		render:function(data,type,row){
			    			var id=row.id;
			    			var fil=row.filetxt;
			    			var voyageDesc=row.nom;//+' - '+row.nomAgence;
			    			var btndelete=`
			    			<a href="#" class="dropdown-item" onClick="deleteRes(this,`+id+`,'`+voyageDesc+`')">
	                        	process with another Algroithme
	                        	&nbsp&nbsp<i class="material-icons pull-right">add</i>
	                        </a>
			    			`;
			    			var btnactivate=`
			    			<a href="#" class="dropdown-item" onClick="ListeImage(this,`+id+`,'`+voyageDesc+`')">
	                        	list of processed images
	                        	&nbsp&nbsp<i class="material-icons pull-right">done</i>
	                        </a>
			    			`;
			    			var btnDetail=`
			    			<a href="#" class="dropdown-item" onClick="Detail(this,`+id+`,'`+voyageDesc+`')">
	                        	Delete
	                        	&nbsp&nbsp<i class="material-icons pull-right">delete</i>
	                        </a>
			    			`;
			    			var btnSimilaire=`
			    			<a href="#" class="dropdown-item" onClick="similaireCBIR(this,`+id+`,'`+voyageDesc+`')">
	                        	See similaire case Using eucleudien Distance
	                        	&nbsp&nbsp<i class="material-icons pull-right">list</i>
	                        </a>
			    			`;
			    			var btnSimilaireCos=`
			    			<a href="#" class="dropdown-item" onClick="similaireCBIRcos(this,`+id+`,'`+voyageDesc+`')">
	                        	See similaire case Using Cos Distance
	                        	&nbsp&nbsp<i class="material-icons pull-right">list</i>
	                        </a>
			    			`;
			    			var btnStep=`
			    			<a href="#" class="dropdown-item" onClick="StepSegmentation(this,`+id+`,'`+voyageDesc+`')">
	                        	See the steps of the segmentation
	                        	&nbsp&nbsp<i class="material-icons pull-right">list</i>
	                        </a>
			    			`;
			    			var ModelCbir=`
			    			<a href="#" class="dropdown-item" onClick="ModelCbir(this,`+id+`,'`+voyageDesc+`')"  data-toggle="modal" data-target="#mainModal">
	                        	See similaire cases
	                        	&nbsp&nbsp<i class="material-icons pull-right">list</i>
	                        </a>
			    			`;
			    			var PerformanceCbir=`
			    			<a href="#" class="dropdown-item" onClick="PerformanceCbir(this,`+id+`,'`+voyageDesc+`')"  data-toggle="modal" data-target="#mainModal">
	                        	CBIR performance for this image
	                        	&nbsp&nbsp<i class="material-icons pull-right">list</i>
	                        </a>
			    			`;
			    			if (fil==""){
			    				btnSimilaire="";
			    			}
					        return `
					        <a class="dropdown">
			                    <a href="#" data-toggle="dropdown" aria-expanded="false">
			                        <i class="material-icons">settings</i>
			                        <div class="ripple-container"></div>
			                    </a>
			                    <div class="dropdown-menu dropdown-menu-right">
			                        <h6 class="dropdown-header">Actions</h6>
			                        `+btnStep+ModelCbir+PerformanceCbir+btnDetail+`
			                        <div class="dropdown-divider"></div>
			                    </div>
			                </a>
					        `;
					    }
			    	}
			    	
			    ]
			});
		}
		 $(document).ready(function(){
			initTable();
			 $('#openRegisterModal').click(function(){
            loadView('/Agences/Inscription','#modalContent');
        });
		});
		 similaireCBIR=function(target,id,voyageDesc) {
		 	window.location.href = "{{ URL::to('/similaireCase/') }}/"+id;
		 }
		 similaireCBIRcos=function(target,id,voyageDesc) {
		 	window.location.href = "{{ URL::to('/similaireCaseCos/') }}/"+id;
		 }
		 StepSegmentation=function(target,id,voyageDesc) {
		 	window.location.href = "{{ URL::to('/SegementationSteps/') }}/"+id;
		 }
		 PerformanceCbir=function(target,id,voyageDesc) {
		 	window.location.href = "{{ URL::to('/Graph/') }}/"+id;
		 }
		 ModelCbir=function(target,id,voyageDesc){
		 	loadView('/CbirResultModel/'+id+'','#modalContent');
		 }
</script>
@endsection
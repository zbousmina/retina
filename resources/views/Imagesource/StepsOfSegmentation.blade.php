@extends('layouts.main')
@section('title',__('patient list'))
@section('app')
<div class="container-fluid">
	<div class="pull-left">
		<h3 style="display: inline;">Steps of the segmentation</h3>
		<img src="{{ URL::asset('/images'.'/'.$ImageSource->image) }}"  height="75" width="75"></img>
	</div>
	<?php
				    $idImage=$ImageSource->id;
				 ?>
	<table id="mainTable" class="display nowrap">
	    <thead>
	        <tr>
	        	<th>Title</th>
	            <th>Image</th>
	            <th>Image</th>
	        </tr>
	    </thead>
	</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var mainTable=0;
	function initTable(){
			mainTable=$('#mainTable').DataTable({
				// "scrollX": true,
				// "autoWidth":false,
				
				"language": {
		            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+
		            (('{{ app()->getLocale() }}'=='fr') ? 'French' : ('{{ app()->getLocale() }}'=='en') ? 'English' : 'Arabic')
		            +".json"
		        },
				ajax: {
			    	url: '{{ URL::to("/api/SegementationSteps"."/".$idImage) }}',
			    	dataSrc: ''
			    },
			    columns:[
			    	{data:'titre'},
			    	{
			    		data:'image',
			    		render:function(data,type,row){
			    			if(data=='') return 'Not traited yet';
					        return `<a href="{{ URL::asset("./images") }}/`+data+`" target="_blank">Click to Download the Image</a>`;
					    }
			    	},
			    	{
			    		data:'image',
			    		render:function(data,type,row){
			    			if(data=='') return 'Not traited yet'; 
					        return '<img src="{{ URL::asset("./images") }}/'+data+'" alt="image '+row.nomAgence+'" height="50" width="50"></img>';
					    }
			    	}
			    ]
			});
		}
		 $(document).ready(function(){
			initTable();
		});
	</script>
@endsection
@extends('layouts.main')
@section('styles')
@endsection
@section('app')
<div class="container">
	<div class="card container-fluid">
		<form class="form" id="TraiterImage" enctype="multipart/form-data">
			@csrf
            <div class="mr-auto" >
                <h2 class="modal-title text-center">{{ $client->nom.'  '.$client->prenom }}</h2>
                <h4 class="modal-title text-center"> Add a new Image</h4>
            </div>
			<div class="image form-group col-md-4" style="border: 1px solid grey;border-radius: 10px;">
			    <label>Put your Image Here</label>
			    <input type="file" name="image" class="form-control-file">
			    <progress value="" max="100"></progress>
			    <div class="text-center err">
			        <div id="imagesindexHelp" class="text-danger"></div>
			    </div>
			</div>
            <div class="col-md-9">
                        <div class="form-group">
                            <label>Note</label>
                            <textarea name="Remarque" class="form-control"></textarea>
                            <div class="text-center err">
                                <div id="RemarqueHelp" class="text-danger"></div>
                            </div>
                        </div>
            </div>
			<div class="modal-footer">
	          <button type="submit" disabled="" class="btn btn-primary pull-right">Process the image</button>
	        </div>
		</form>
	</div>
	<!-- Resultas -->
	<div id="Resultas" class="col-md-10">

	</div>
	<br>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
		$('#TraiterImage [type="submit"]').removeAttr('disabled');
	});	
var spinner=`@include('layouts.spinner')`;
$('#TraiterImage').submit((e)=>{
        e.preventDefault();
        $('#TraiterImage [type=submit]').html(spinner);
       
		$('#TraiterImage [type=submit]').attr('disabled','disabled');
        doTraiteImage($('#TraiterImage'))
        .then(()=>{
        	$('#TraiterImage [type=submit]').removeAttr('disabled');
        	$('#TraiterImage [type=submit]').html('Traiter');
        }).catch(()=>{
        	$('#TraiterImage [type=submit]').removeAttr('disabled');
        	$('#TraiterImage [type=submit]').html('Traiter');
        	alert('an error occured, please verify your network connection');
        });
    });
function validate(errors,form,element){
        if(errors[element]){
            $('#'+form+' [name='+element+']').closest('.input-group').addClass('has-danger');
            $('#'+form+' #'+element+'-error').remove();
            $('#'+form+' #'+element+'Help')
                .append('<span id="'+element+'-error" class="help-block help-block-error">'+errors[element][0]+'</span>');
        }else{
            unhighlight(form,element);
        }
    }
    function unhighlight(form,element){
        $('#'+form+' #'+element+'-error').remove();
        $('#'+form+' [name='+element+']').closest('.input-group').removeClass('has-danger');
    }

async function doTraiteImage(form){
var f=document.getElementById('TraiterImage');
await $.ajax({
    <?php
                    $idcl=$client->id;
                 ?>
            url:"{{ URL::to('Image/AddSupervised'.'/'.$idcl) }}",
            method:'POST',
            data:new FormData(f),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            $('progress').attr({value: e.loaded,max: e.total,});
                        }
                    }, false);
                }
                return myXhr;
            },
            success:function(data) {
            	var errors=data.error;
            	if(typeof errors!='undefined'){
            		validate(errors,'TraiterImage','image');
                    validate(errors,'TraiterImage','Remarque');
            	if(errors[Object.keys(errors)[0]])
                    toast.alert(errors[Object.keys(errors)[0]]);
                else{
                    toast.alert('Erreur dans le traitement');
                }
            }else{//no validation errors
                    unhighlight('TraiterImage','image');
                    unhighlight('TraiterImage','Remarque');
                    toast.success("Your image will be traited");
                    window.location.replace("{{URL::to('ImageClient'.'/'.$idcl)}}");
                }
            }
        });
	}
</script>
@endsection	

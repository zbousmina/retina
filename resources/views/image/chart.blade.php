@extends('layouts.main')
@section('styles')
@endsection
@section('app')
<div class="container">
	<div id="chartContainer" style="height: 300px; max-width: 920px; margin: 0px auto;"></div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var idImageTreited='{{$ImageTraited->id}}';
	window.onload = function () {
	
	GetPGC();

}
function toggleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else{
		e.dataSeries.visible = true;
	}
	chart.render();
}
var dataPGC=[];
var dataPGE=[];
var dataPHC=[];
var dataPHE=[];
var dataSGC=[];
var dataSGE=[];
var dataSHC=[];
var dataSHE=[];
async function GetPGC(){
	await $.ajax({
            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/PGC",
            method:'GET',
            dataType:'JSON',
            success:function(data) {
            	dataPGC=data;
            	$.ajax({
		            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/PGE",
		            method:'GET',
		            dataType:'JSON',
		            success:function(data) {
		            	dataPGE=data;
						$.ajax({
				            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/PHC",
				            method:'GET',
				            dataType:'JSON',
				            success:function(data) {
				            	dataPHC=data;
				            	$.ajax({
						            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/PHE",
						            method:'GET',
						            dataType:'JSON',
						            success:function(data) {
						            	dataPHE=data;
						            	$.ajax({
								            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/SGE",
								            method:'GET',
								            dataType:'JSON',
								            success:function(data) {
								            	dataSGE=data;
								           		$.ajax({
										            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/SGC",
										            method:'GET',
										            dataType:'JSON',
										            success:function(data) {
										            	dataSGC=data;
										            	$.ajax({
												            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/SHC",
												            method:'GET',
												            dataType:'JSON',
												            success:function(data) {
												            	dataSHC=data;
												            	$.ajax({
														            url:"{{ URL::to('api/GraphPGC') }}/"+idImageTreited+"/SHE",
														            method:'GET',
														            dataType:'JSON',
														            success:function(data) {
														            	dataSHE=data;
														            	var chart = new CanvasJS.Chart("chartContainer", {
																			animationEnabled: true,
																				title:{
																					text: "CBIR performance measurement"
																				},
																				axisX: {
																					title: "Nb Result"
																				},
																				axisY: {
																					title: "Percentage",
																					suffix: " %"
																				},
																				legend:{
																					cursor: "pointer",
																					fontSize: 16,
																					itemclick: toggleDataSeries
																				},
																				toolTip:{
																					shared: true
																				},
																				data: [{
																					name: "PGC",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataPGC
																				},
																				{
																					name: "PGE",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataPGE
																				},
																				{ 	
																					name: "PHC",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataPHC
																				},
																				{
																					name: "PHE",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataPHE
																				},
																				{
																					name: "SGC",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataSGC
																				},
																				{
																					name: "SGE",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataSGE
																				},
																				{
																					name: "SHC",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataSHC
																				},
																				{
																					name: "SHE",
																					type: "line",
																					yValueFormatString: "#0.## %",
																					showInLegend: true,
																					dataPoints: dataSHE
																				},
																				]
																			});
														            		chart.render();
														            		}
														            	});
												            		}
												            	});
										            		}
										            	});
										            }	
								            	});
						            		}
						            	});
				            		}
				            	});	
				            }	            	
		            	});
            		}		
            
        });
}
</script>
@endsection	
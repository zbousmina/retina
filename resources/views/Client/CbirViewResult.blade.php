<nav class="navbar navbar-expand-lg bg-rose" >
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">IMAGE TYPE</a>
                </li>
                <li class="nav-item">
                    <select id="TypeImage" type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="1">Original</option>
                        <option value="2">Segemented</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">FEATURE TYPE</a>
                </li>
                <li class="nav-item">
                    <select id="TypeFeatures" type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="G">GLCM</option>
                        <option value="H">HOG</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">DETECTION METHOD</a>
                </li>
                <li class="nav-item">
                    <select id="TypeSegmentation" type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="P">Proposed</option>
                        <option value="S">Supervised</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">NUMBER OF IMG</a>
                </li>
                <li class="nav-item">
                    <select id="nbImage" type="text" name="NbImage" style="border-radius: 25px;">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                    </select>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">DISTANCE</a>
                </li>
                <li class="nav-item">
                    <select id="TypeDistance" type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="E">Euclidean</option>
                        <option value="C">Cosine</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
	<div class="container-fluid col-md-2" >
		<div align="center">0.00</div>
		<img id="InputImage" src="{{ URL::asset('/images'.'/'.$ImageSource->image) }}" height="150" width="150" style="padding: 7px; background-color: {{$ImageSource->color}};"></img>
		<div align="center">Img 0</div>
	</div>
</div>
<div class="modal-body" id="modelResult">
	
</div>
<script type="text/javascript">
    var inputImage=$('#InputImage');
	var rowImage=`@include('Imagesource.imageRow')`;
	var spinner=`@include('layouts.spinner')`;
	var imageCollumn=`@include('Imagesource.ImageCollumn')`;
    var imgOriginal="{{URL::asset('/images'.'/'.$ImageSource->image)}}";
    var imgSegmentedPropos="{{URL::asset('/images'.'/'.$ImageSource->finaleSegementeProposed)}}";
    var imgSegmentedSupervide="{{URL::asset('/images'.'/'.$ImageSource->finaleSegementeSupervised)}}";
	async function getResutl(id,Algo,nb,type){
    	await $.ajax({
            url:"{{ URL::to('api/CbirResult') }}/"+id+"/"+Algo+"/"+nb+"/"+type,
            method:'GET',
            dataType:'JSON',
            success:function(data) {
	            $('#modelResult').html('');
	            var j=1;
	            var idRow="";
                document.getElementById('InputImage').style.backgroundColor=data[0].color;
                if (type==1){
                    document.getElementById('InputImage').src=imgOriginal;
                }else{
                    if(Algo[0]=='P'){
                    document.getElementById('InputImage').src=imgSegmentedPropos;
                    }else{
                    document.getElementById('InputImage').src=imgSegmentedSupervide;
                    }
                }
	            for (var i = 0; i < data.length; i++) {
	            	if (i%5==0){
	            		var opeeRow=rowImage.replace(/index/g,j);
	            		$('#modelResult').append(opeeRow);
	            		idRow="row"+j;
	            		j++;
	            	}
	            	var offree=data[i];
	            	var o=imageCollumn.replace(/Color/g,offree.color);
	            	var o=o.replace(/index/g,offree.id);
	            	$('#'+idRow).append(o);
	            	$('#'+idRow+' #imageTitle'+offree.id).html(''+offree.title);
	            	$('#'+idRow+' #Distance'+offree.id).html(''+offree.distance);
	            	$('#'+idRow+' #imagesrc'+offree.id).attr('src','{{ URL::to(".") }}/'+offree.image);
	            }
            }
        });
    }
    $(document).ready(function(){
        <?php
            $idImage=$ImageSource->id;
        ?>
        var idpremiere='{{$idImage}}';
    	getResutl(idpremiere,"PGE",5,1);
        var typeImage=$('#TypeImage');
        var typeSegmentation=$('#TypeSegmentation');
        var typeFeatures=$('#TypeFeatures');
        var typeDistance=$('#TypeDistance');
        var nbImage=$('#nbImage');
        
        
        $(typeImage).on('click',(e)=>{
            var Algorith=typeSegmentation[0].value+typeFeatures[0].value+typeDistance[0].value;
            getResutl(idpremiere,Algorith,nbImage[0].value,typeImage[0].value);

        });
        $(typeSegmentation).on('click',(e)=>{
            var Algorith=typeSegmentation[0].value+typeFeatures[0].value+typeDistance[0].value;
            getResutl(idpremiere,Algorith,nbImage[0].value,typeImage[0].value);
        });
        $(typeFeatures).on('click',(e)=>{
            var Algorith=typeSegmentation[0].value+typeFeatures[0].value+typeDistance[0].value;
            getResutl(idpremiere,Algorith,nbImage[0].value,typeImage[0].value);
        });
        $(typeDistance).on('click',(e)=>{
            var Algorith=typeSegmentation[0].value+typeFeatures[0].value+typeDistance[0].value;
            getResutl(idpremiere,Algorith,nbImage[0].value,typeImage[0].value);
        });
        $(nbImage).on('click',(e)=>{
            var Algorith=typeSegmentation[0].value+typeFeatures[0].value+typeDistance[0].value;
            getResutl(idpremiere,Algorith,nbImage[0].value,typeImage[0].value);
        });

    })

</script>
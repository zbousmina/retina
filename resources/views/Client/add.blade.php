@extends('layouts.main')
@section('styles')
@endsection
@section('app')
<div class="container">
	<div class="card container-fluid">
		<form class="form" id="addCharterForm" enctype="multipart/form-data">
			@csrf
			<div class="mr-auto" >
	            <h2 class="modal-title text-center">Add patient</h2>
	        </div>
	        <div class="row">
		        <div class="col-md-4">
	                <div class="form-group">
	                    <label>last Name</label>
	                    <input type="text" name="nom" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
	                    <div class="text-center err">
	                        <div id="nomHelp" class="text-danger"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label>first Name</label>
	                    <input type="text" name="prenom" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
	                    <div class="text-center err">
	                        <div id="prenomHelp" class="text-danger"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label>email</label>
	                    <input type="text" name="email" class="form-control">
	                    <div class="text-center err">
	                        <div id="emailHelp" class="text-danger"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label>mobile</label>
	                    <input type="text" name="mobile" class="form-control">
	                    <div class="text-center err">
	                        <div id="mobileHelp" class="text-danger"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label>Phone</label>
	                    <input type="text" name="telephone" class="form-control">
	                    <div class="text-center err">
	                        <div id="telephoneHelp" class="text-danger"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-9">
		                <div class="form-group">
		                    <label>Note</label>
		                    <textarea name="Remarque" class="form-control"></textarea>
		                    <div class="text-center err">
		                        <div id="RemarqueHelp" class="text-danger"></div>
		                    </div>
		                </div>
		        </div>
	        </div>
	        <div class="modal-footer">
	          <button type="submit" disabled="" class="btn btn-primary pull-right">Add</button>
	          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
	        </div>
	    </form>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('#addCharterForm [type="submit"]').removeAttr('disabled');
	});
	var spinner=`@include('layouts.spinner')`;
	function validate(errors,form,element){
        if(errors[element]){
            $('#'+form+' [name='+element+']').closest('.input-group').addClass('has-danger');
            $('#'+form+' #'+element+'-error').remove();
            $('#'+form+' #'+element+'Help')
                .append('<span id="'+element+'-error" class="help-block help-block-error">'+errors[element][0]+'</span>');
        }else{
            unhighlight(form,element);
        }
    }
    function unhighlight(form,element){
        $('#'+form+' #'+element+'-error').remove();
        $('#'+form+' [name='+element+']').closest('.input-group').removeClass('has-danger');
    }
    $('#addCharterForm').submit((e)=>{
        e.preventDefault();
        $('#addCharterForm [type=submit]').html(spinner);
		$('#addCharterForm [type=submit]').attr('disabled','disabled');
        DoaddCharter($('#addCharterForm'))
        .then(()=>{
        	$('#addCharterForm [type=submit]').removeAttr('disabled');
        	$('#addCharterForm [type=submit]').html('Ajouter');
        }).catch(()=>{
        	$('#addCharterForm [type=submit]').removeAttr('disabled');
        	$('#addCharterForm [type=submit]').html('Ajouter');
        	alert('an error occured, please verify your network connection');
        });
    });
    async function DoaddCharter(form){
    	var f=document.getElementById('addCharterForm');
    	await $.ajax({
            url:"{{ URL::to('Client/Add') }}",
            method:'POST',
            data:new FormData(f),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            // Custom XMLHttpRequest
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            $('progress').attr({value: e.loaded,max: e.total,});
                        }
                    }, false);
                }
                return myXhr;
            },
            success:function(data) {
                var errors=data.error;
                if(typeof errors!='undefined'){//there were validation errors
                    validate(errors,'addCharterForm','nom');
                    validate(errors,'addCharterForm','prenom');
                    validate(errors,'addCharterForm','email');
                    validate(errors,'addCharterForm','mobile');
                    validate(errors,'addCharterForm','telephone');
                    validate(errors,'addCharterForm','Remarque');
                    if(errors[Object.keys(errors)[0]])
                    	toast.alert(errors[Object.keys(errors)[0]]);
                    else{
                    	toast.alert("A an error please try again");
                    }
                }else{//no validation errors
                    unhighlight('addCharterForm','nom');
                    unhighlight('addCharterForm','prenom');
                    unhighlight('addCharterForm','email');
                    unhighlight('addCharterForm','mobile');
                    unhighlight('addCharterForm','telephone');
                    unhighlight('addCharterForm','Remarque');
                    $('#modalContent').html("");
                    $('#mainModal').modal('hide');
                    toast.success("Patient well added");
                    window.location.replace("{{URL::to('Clients')}}");
                }
            }
        });
    }
</script>
@endsection

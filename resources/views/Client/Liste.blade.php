@extends('layouts.main')
@section('title',__('patient list'))
@section('app')
<div class="container-fluid">
	<div class="pull-left">
		<h3 style="display: inline;">List my patients :   </h3>

		<a href="{{ URL::to('Client/Add') }}" class="btn btn-rose btn-raised btn-round">
			Add patient
		</a>
	</div>
	<table id="mainTable" class="display nowrap">
	    <thead>
	        <tr>
	        	<th>Last Name</th>
	            <th>first name</th>
	            <th>phone</th>
	            <th>Email</th>
	            <th>mobile</th>
	            <th>Note</th>
	            <th>action</th>
	        </tr>
	    </thead>
	</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
		var mainTable=0;
		
		function initTable(){
			mainTable=$('#mainTable').DataTable({
				// "scrollX": true,
				// "autoWidth":false,
				"language": {
		            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/"+
		            (('{{ app()->getLocale() }}'=='fr') ? 'French' : ('{{ app()->getLocale() }}'=='en') ? 'English' : 'Arabic')
		            +".json"
		        },
				ajax: {
			    	url: '{{ URL::to("/api/MyClients") }}',
			    	dataSrc: ''
			    },
			    columns:[
			    	{data:'nom'},
			    	{data:'prenom'},
			    	{data:'telephone'},
			    	{data:'email'},
			    	{data:'mobile'},
			    	{data:'Remarque'},
			    	{
			    		data:'id',
			    		render:function(data,type,row){
			    			var id=row.id;
			    			var voyageDesc=row.nom;//+' - '+row.nomAgence;
			    			var btndelete=`
			    			<a href="#" class="dropdown-item" onClick="deleteRes(this,`+id+`,'`+voyageDesc+`')">
	                        	process a new image
	                        	&nbsp&nbsp<i class="material-icons pull-right">add</i>
	                        </a>
			    			`;
			    			var btnactivate=`
			    			<a href="#" class="dropdown-item" onClick="ListeImage(this,`+id+`,'`+voyageDesc+`')">
	                        	list of processed images
	                        	&nbsp&nbsp<i class="material-icons pull-right">done</i>
	                        </a>
			    			`;
			    			var btnSendDocs=`
			    			<a href="#" class="dropdown-item" onClick="UploadDocument(this,`+id+`,'`+voyageDesc+`')">
	                        	Update patient
	                        	&nbsp&nbsp<i class="material-icons pull-right">edit</i>
	                        </a>
			    			`;
			    			var btnDetail=`
			    			<a href="#" class="dropdown-item" onClick="Detail(this,`+id+`,'`+voyageDesc+`')">
	                        	Delete
	                        	&nbsp&nbsp<i class="material-icons pull-right">delete</i>
	                        </a>
			    			`;
					        return `
					        <a class="dropdown">
			                    <a href="#" data-toggle="dropdown" aria-expanded="false">
			                        <i class="material-icons">settings</i>
			                        <div class="ripple-container"></div>
			                    </a>
			                    <div class="dropdown-menu dropdown-menu-right">
			                        <h6 class="dropdown-header">Actions</h6>
			                        `+btnactivate+btnSendDocs+btndelete+btnDetail+`
			                        <div class="dropdown-divider"></div>
			                    </div>
			                </a>
					        `;
					    }
			    	}
			    	
			    ]
			});
		}
		 $(document).ready(function(){
			initTable();
		});
		ListeImage=function(target,id,voyageDesc){
			window.location.href = "{{ URL::to('/ImageClient/') }}/"+id;
		} 
</script>
@endsection
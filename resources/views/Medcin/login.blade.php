@extends('layouts.blank_login')
@section('title',__('Login'))
@section('app')
<div class="container">
  <div class="row">
    <div class="col-lg-4 col-md-6 ml-auto">
      <style type="text/css">
        .card-login i,.card-login input{
          color: white!important;
        }
        .card-login ::placeholder {
          color: white;
          /*opacity: 0.7;*/
        }
        .err div {
          color:rgb(255,70,70)!important;
          /*font-weight: bold!important;*/
          font-size: 13px!important;
        }
        .card-body{
          height: auto!important;
        }
      </style>
      <div class="card card-login" style="background-color: rgba(0,60,191,0.6)">
        <form class="form" action="/Login" id="loginForm">
            @csrf
            <br>
            <br>
            <br>
            <br>
          <p class="description text-center"> </p>
          <div class="card-body" style="margin-top: 10%;">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">mail</i>
                </span>
              </div>
              <input type="email" name="email" class="form-control" placeholder="Enter your email">
            </div>
            <div class="text-center err">
                <div id="emailHelp" class="text-danger"></div>
            </div>
            <div class="input-group" style="margin-top: 20%;">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">lock_outline</i>
                </span>
              </div>
              <input type="password" name="password" class="form-control" placeholder="enttrer your password">
            </div>
            <div class="text-center err">
                <div id="passwordHelp" class="text-danger"></div>
            </div>
            <a class="text-light" href="{{ URL::to('ForgotPassword') }}">Forgot your password ?</a>
          </div>
          <div class="footer text-center">
            <button type="submit" class="btn btn-primary btn-wd btn-lg">Login
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@include('layouts.modal')
@endsection


@section('scripts')
<!-- <script src="../../assets/pages/sessions/login.js"></script> -->
<script type="text/javascript">
    $(document).ready(()=>{
        $('#loginForm button').removeAttr('disabled');
        $('#openRegisterModal').click(function(){
            loadView('/Agences/Inscription','#modalContent');
        });
    });
    var spinner=`@include('layouts.spinner')`;
    $('#loginForm').submit((e)=>{
        e.preventDefault();
        $('#loginForm [type=submit]').html(spinner);
        doLogin($('#loginForm')).then(()=>{
          $('#loginForm [type=submit]').html('{{ __('strings.login') }}');
        });
    });
    function validate(errors,form,element){
        if(errors[element]){
            $('#'+form+' [name='+element+']').closest('.input-group').addClass('has-danger');
            $('#'+form+' #'+element+'-error').remove();
            $('#'+form+' #'+element+'Help')
                .append('<span id="'+element+'-error" class="help-block help-block-error">'+errors[element][0]+'</span>');
        }else{
            unhighlight(form,element);
        }
    }
    function unhighlight(form,element){
        $('#'+form+' #'+element+'-error').remove();
        $('#'+form+' [name='+element+']').closest('.input-group').removeClass('has-danger');
    }
    async function doLogin(form){
        await $.post(
            "{{ URL::to('Login') }}",
            form.serializeArray(),
            function(data) {
                var errors=data.error;
                if(typeof errors!='undefined'){//there were validation errors
                    validate(errors,'loginForm','email');
                    validate(errors,'loginForm','password');
                    toast.alert("{{ __('strings.loginError') }}");
                }else{//no validation errors
                    unhighlight('loginForm','email');
                    unhighlight('loginForm','password');
                    //login success
                    toast.success("{{ __('strings.loginSuccess') }}");
                    window.location.href = "{{ URL::to('/Clients') }}";
                }
            }
        );
    }

</script>
@endsection
<div class="modal-header text-center">
  <h5 class="modal-title text-center">Formulaire d'enregistrement</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <i class="material-icons">clear</i>
  </button>
</div>
<div class="modal-body">
    <form class="form" id="registrationForm" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>last Name</label>
                    <input type="text" name="nom" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
                    <div class="text-center err">
                        <div id="nomHelp" class="text-danger"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>first Name</label>
                    <input type="text" name="prenom" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
                    <div class="text-center err">
                        <div id="prenomHelp" class="text-danger"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>email</label>
                    <input type="text" name="email" class="form-control">
                    <div class="text-center err">
                        <div id="emailHelp" class="text-danger"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>mobile</label>
                    <input type="text" name="mobile" class="form-control">
                    <div class="text-center err">
                        <div id="mobileHelp" class="text-danger"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" placeholder="enttrer your password">
                <div class="text-center err">
                        <div id="passwordHelp" class="text-danger"></div>
                </div>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
              <label>Password Confirmation</label>
              <input type="password" name="passwordConfirmation" class="form-control" placeholder="Confirmation">
                <div class="text-center err">
                        <div id="passwordConfirmationHelp" class="text-danger"></div>
                </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary pull-right">register</button>
          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">close</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $('#registrationForm').submit((e)=>{
        e.preventDefault();
        $('#registrationForm [type=submit]').html(spinner);
        doRegister($('#registrationForm')).then(()=>{
          $('#registrationForm [type=submit]').html('register');
        });
    });
    async function doRegister(form){
        var f=document.getElementById('registrationForm');
        //data.push({name:'logo',value:$('#registrationForm [type=file]')[0].files[0]});
        await $.ajax({
            url:"{{ URL::to('Agences/Inscription') }}",
            method:'POST',
            data:new FormData(f),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            // Custom XMLHttpRequest
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            $('progress').attr({value: e.loaded,max: e.total,});
                        }
                    }, false);
                }
                return myXhr;
            },
            success:function(data) {
                var errors=data.error;
                if(typeof errors!='undefined'){//there were validation errors
                    validate(errors,'registrationForm','nom');
                    validate(errors,'registrationForm','prenom');
                    validate(errors,'registrationForm','email');
                    validate(errors,'registrationForm','mobile');
                    validate(errors,'registrationForm','password');
                    validate(errors,'registrationForm','passwordConfirmation');
                    toast.alert("{{ __('strings.registerError') }}");
                }else{//no validation errors
                    unhighlight('registrationForm','nom');
                    unhighlight('registrationForm','prenom');
                    unhighlight('registrationForm','email');
                    unhighlight('registrationForm','mobile');
                    unhighlight('registrationForm','password');
                    unhighlight('registrationForm','passwordConfirmation');
                    //register success
                    $('#modalContent').html("");
                    $('#mainModal').modal('hide');
                    toast.success("{{ __('strings.registerSuccess') }}");
                }
            }
        });
    }
</script>
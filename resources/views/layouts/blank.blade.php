<!DOCTYPE html>
<html>
<head>
	<meta charset = "utf-8">
	<title> @yield('title') - Thoriya</title>
	@include('layouts.basic_imports')
	@yield('styles')
</head>
<body class="index-page sidebar-collapse">
	@yield('app')
	@include('layouts.basic_js')
	@yield('scripts')
</body>
</html>
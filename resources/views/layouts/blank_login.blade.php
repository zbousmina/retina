<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset = "utf-8">
    <title> EYE SAVER</title>
	@include('layouts.basic_imports')
	@yield('styles')
</head>
<body class="login-page sidebar-collapse">
	@include('layouts.header_transparent')
	<div class="page-header header-filter" style="background-image: url('../assets/img/eye-exam.jpg'); background-size: cover; background-position: top center;">
		@yield('app')
	</div>
	@include('layouts.footer')
@include('layouts.basic_js')
@yield('scripts')
</body>
</html>
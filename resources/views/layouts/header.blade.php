<!--        rose navbar with search form -->
<nav class="navbar navbar-expand-lg bg-rose" >
    <div class="container">
        <div class="navbar-translate">
            <img src="http://127.0.0.1:8000/assets/img/Header.svg" height="50px" width="122px" style="border: 0px; margin: 0px; padding: 0px;">
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ URL::to('Clients') }}" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::to('Clients') }}" class="nav-link">List my patients</a>
                </li>
                
                <li class="nav-item">
                    <li class="dropdown nav-item">
                        <a href="" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false">
                            list of processed images
                            <b class="caret"></b>
                            <div class="ripple-container"></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <h6 class="dropdown-header">list of processed images</h6>
                            <a href="#" class="dropdown-item">This week</a>
                            <a href="#" class="dropdown-item">This mounth</a>
                            <a href="#" class="dropdown-item">all</a>
                        </div>
                    </li>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::to('Clients') }}" class="nav-link">Algorithms</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="dropdown nav-item">
                    <a href="" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false">
                        <i class="material-icons">account_circle</i>
                        <b class="caret"></b>
                        <div class="ripple-container"></div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <h6 class="dropdown-header">account setting</h6>
                        <a href="#" class="dropdown-item">My account</a>
                        <div class="dropdown-divider"></div>
                        <a href="/Logout" class="dropdown-item">
                            Logout
                            &nbsp&nbsp<i class="material-icons pull-right">logout</i>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--        end rose navbar -->
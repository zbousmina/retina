<!--        rose navbar with search form -->
<nav class="navbar navbar-expand-lg bg-rose" >
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">IMAGE TYPE</a>
                </li>
                <li class="nav-item">
                    <select type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="1">Original</option>
                        <option value="2">Segemented</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">FEATURE TYPE</a>
                </li>
                <li class="nav-item">
                    <select type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="1">GLCM</option>
                        <option value="2">HOG</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">DETECTION METHOD</a>
                </li>
                <li class="nav-item">
                    <select type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="1">Supervised</option>
                        <option value="2">Proposed</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
                
                
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">NUMBER OF IMG</a>
                </li>
                <li class="nav-item">
                    <select type="text" name="NbImage" style="border-radius: 25px;">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                    </select>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">DISTANCE</a>
                </li>
                <li class="nav-item">
                    <select type="text" name="TypeImage" style="border-radius: 25px;">
                        <option value="1">Euclidean</option>
                        <option value="2">Cosine</option>
                    </select>
                    <div class="text-center err">
                        <div id="allerpaysArrivee_idHelp" class="text-danger"></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--        end rose navbar -->
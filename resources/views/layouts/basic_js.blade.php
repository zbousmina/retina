	<!--   Core JS Files   -->
	<script src="{{ URL::asset('./assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('./assets/js/core/popper.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('./assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('./assets/js/plugins/moment.min.js') }}"></script>
	<!-- moment js translation -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js" type="text/javascript"></script>
	<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
	<script src="{{ URL::asset('./assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="{{ URL::asset('./assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
	<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
	<script src="{{ URL::asset('./assets/js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
	<!-- DataTables -->
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<!-- dataTables extensions -->
	<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js" type="text/javascript"></script>
	<!-- end dataTables extensions -->
	<!-- toast -->
	<!-- import js -->
	<script src="https://cdn.jsdelivr.net/npm/siiimple-toast/dist/siiimple-toast.min.js"></script>
	<!-- sweet alert -->
	<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<!-- custom js -->
	<script src="{{ URL::asset('./js/router.js') }}" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162458567-1"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-162458567-1');
	</script>

	<script type="text/javascript">
		var toast=siiimpleToast.setOptions({
		  container: 'body',
		  class: 'siiimpleToast',
		  position: 'top|center',
		  margin: 15,
		  delay: 0,
		  duration: 3000,
		  style: {},
		});
		window.initDateTimePicker=function(){
	    	//init datepickers
		    $('.datepicker').datetimepicker({
	            format: 'YYYY-MM-DD',
	            defaultDate: moment().add(1,'days'),
	            // locale: '{{ app()->getLocale() }}',
	            locale: 'fr',
	            icons: {
			        time: "fa fa-clock-o",
			        date: "fa fa-calendar",
			        up: "fa fa-chevron-up",
			        down: "fa fa-chevron-down",
			        previous: 'fa fa-chevron-left',
			        next: 'fa fa-chevron-right',
			        today: 'fa fa-screenshot',
			        clear: 'fa fa-trash',
			        close: 'fa fa-remove'
			    }
	        });
	        $('.timepicker').datetimepicker({
	            format: 'LT',
	            defaultDate: moment().add(1,'days'),
	            // locale: '{{ app()->getLocale() }}',
	            locale: 'fr',
	            icons: {
			        time: "fa fa-clock-o",
			        date: "fa fa-calendar",
			        up: "fa fa-chevron-up",
			        down: "fa fa-chevron-down",
			        previous: 'fa fa-chevron-left',
			        next: 'fa fa-chevron-right',
			        today: 'fa fa-screenshot',
			        clear: 'fa fa-trash',
			        close: 'fa fa-remove'
			    }
	        });
	    }
		$(document).ready(function(){
  			$("input").inputmask();
  			//init DateTimePickers
			// materialKit.initFormExtendedDatetimepickers();
			window.initDateTimePicker();
		});
		for(var i=1;i<=40;i++){
			setTimeout(function() {
				$('.buttons-excel').html('<i class="material-icons pull-right">table_chart</i>')
				$('.buttons-print').html('<i class="material-icons pull-right">print</i>')
			},i*100)
		}
	</script>
	<style type="text/css">
		.siiimpleToast{
			margin-top: 300px;
		}
	</style>
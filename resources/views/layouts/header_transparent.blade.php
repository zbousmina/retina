<nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
  <div class="container">
    <div class="navbar-translate">
      <img src="../assets/img/Login.svg" height="69px" width="170">
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <button id="openRegisterModal" class="btn btn-rose btn-raised btn-round" data-toggle="modal" data-target="#mainModal">
            <i class="material-icons">library_books</i> register
            <div class="ripple-container"></div></a>
          </button>
        </li>
      </ul>
    </div>
  </div>
</nav>
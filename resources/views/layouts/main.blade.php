<!DOCTYPE html>
<html>
<head>
	<meta charset = "utf-8">
	<title> @yield('title') - EYE SAVER</title>
	@include('layouts.basic_imports')
	@yield('styles')
</head>
<body class="index-page sidebar-collapse">
	@include('layouts.header')
	@yield('app')
	@include('layouts.basic_js')
	<script type="text/javascript">
		function toMoney(money) {
			money=parseInt(money);
			return money.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
		}
	</script>
	@yield('scripts')
</body>
</html>